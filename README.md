# Nyelvek típusrendszere tantárgy (IPM-18sztNYTR), 2020. ősz, ELTE-IK, Programtervező informatikus MSc #


#### Időpontok

- Előadás (Kaposi Ambrus): kedd 16:00-17:30
- Gyakorlat, 1. kurzus (Donkó István): hétfő 17:45-19:15
- Gyakorlat, 2. kurzus (Donkó István): hétfő 19:30-21:00


#### Elérhetőségek

- Kaposi Ambrus, email: akaposi @ inf.elte.hu (szóköz nélkül).
- Donkó István, email: isti115 @ inf.elte.hu (szóköz nélkül).


#### Helyszínek

- Microsoft Teams: az előadások és a gyakorlatok is itt történnek. Az
  inf-es azonosítóval kell belépni. Ha valakinek nem sikerül, írjon a
  Teams-en üzenetet!

- Canvas: az első előadáson kívül minden előadásra meg kell csinálni
  egy Canvas kvízt, ami az előző előadásról szól. Neptun azonosítóval
  lehet belépni.

- Bead rendszer: az elsőn kívül minden gyakorlatra lesz egy beadandó
  és minden gyakorlat elején lesz egy 10 perces teszt, amit a bead
  rendszerbe kell feltölteni (itt automatikusan tesztelve
  lesznek). Regisztrálni kell inf-es azonosítóval.


#### Technikai követelmények

Az [Agda programozási
nyelvet](https://wiki.portal.chalmers.se/agda/pmwiki.php) fel kell
telepíteni, ezt fogjuk használni gyakorlaton és részben előadáson is.


#### Előzetes ismeretek

A BSc-s "Funkcionális programozás" tárgy előkövetelmény, aki nem az
ELTE-re járt BSc-re, az felveheti.


#### Követelmények

- Gyakorlati jegyet az kap, akinek el lett fogadva a nagybeadandója (a
  félév vége felé lesz kiírva). A gyakorlati jegy a gyakorlatok elején
  történő bead-os feladatokból jön össze. A ponthatárok az alábbi
  táblázatban láthatóak:

  | Pont  | Jegy |
  |------:|------|
  |     6 | 2    |
  |   7-8 | 3    |
  |  9-10 | 4    |
  | 11-12 | 5    |

- Vizsgára az jöhet, akinek van nem-egyes gyakorlati jegye és a Canvas
  tesztek Canvas által kiszámolt átlaga 50% fölött van.

- A vizsga részben Canvas kvíz lesz, részben pedig Agda-ban
  lesz. Vizsgán tetszőleges segédeszköz/internet használható,
  kommunikáció más személyekkel viszont nem engedélyezett.


#### Irodalom

Az előadások anyagai a folyamatosan frissülő [jegyzetben](main.pdf) megtalálhatók lesznek.


[Korábbi évek jegyzete](https://bitbucket.org/akaposi/tipusrendszerek/raw/HEAD/jegyzet.pdf?at=master&fileviewer=file-view-default)

[Régi példa-vizsga](https://bitbucket.org/akaposi/tipusrendszerek/raw/master/peldaVizsga.pdf).

[Robert Harper: Practical foundations of programming languages](https://www.cs.cmu.edu/~rwh/pfpl/2nded.pdf)

[Simon Castellan, Pierre Clairambault, and Peter Dybjer: Categories with Families: Unityped, Simply Typed, and Dependently Typed](https://arxiv.org/pdf/1904.00827.pdf)



#### Záróvizsga tematika

Az MSc-s záróvizsga tételsorának S0/4 tétele szól erről a tantárgyról. Korábban ezt a tárgyat Csörnyei Zoltán, Diviánszky Péter és Páli Gábor oktatta, kicsit más tematikával. Az S0/4 tétel jelenleg az új és a régi tananyag metszetét tartalmazza.

Akik a tantárgyat nem nálam végezték, azoknak ideírom, hogy Csörnyei Zoltán könyvében (Bevezetés a típusrendszerek elméletébe, ELTE Eötvös Kiadó, 2012) és Páli Gábor [diáin](http://people.inf.elte.hu/pgj/nytr_msc/) a tételsor anyaga az alábbi módon található meg.

* Absztrakt szintaxisfák, absztrakt kötéses fák, levezetési fák. Csörnyei 2. fejezet. Páli Gábor 1. előadás és 2. diasor a 14. diáig.
* Szintaxis, típusrendszer, operációs szemantika. Ezek bemutathatók például az F1 típusrendszeren. Csörnyei 3.1-3.4. alfejezetek. Páli Gábor 2. diasor, 15-28. dia.
* Típusrendszer és operációs szemantika kapcsolata: haladás és típusmegőrzés tétele. Csörnyei 3.4.22 és 3.4.13 tételek. Páli Gábor 2. diasor, 29. dia.
* Magasabbrendű függvények, Church típusrendszere. Más néven F1 típusrendszer. Csörnyei 3.1-3.4,3.8 alfejezetek. Páli Gábor 2. diasor, 15-30. dia.
* Let kifejezések. Csörnyei 5.2.1. Páli Gábor 6. diasor, 4-5. dia.
* Szorzat és összeg típusok. Csörnyei 3.5.1, 3.5.4, 3.5.5, 3.5.8, 3.5.9. Páli Gábor 3. diasor.
* Induktív típusok: Bool, természetes számok. Csörnyei 3.5.2, 3.5.3, 3.5.7, 3.6. Páli Gábor 3. diasor és 4. diasor a 6. diáig.
* Polimorfizmus (System F), absztrakt típusok. Csörnyei 6. fejezet. Páli Gábor 8. diasor, 9. diasor.
* Altípus. Csörnyei 3.7 fejezet. Páli Gábor 4. diasor 7-18. dia.
