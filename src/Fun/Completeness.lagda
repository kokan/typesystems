\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
\end{code}
\begin{code}
module Fun.Completeness where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import Fun.Algebra
open import Fun.Standard

open I
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

Comp : DepAlgebra
Comp = record
         { Con = λ Γ' → Sub ∙ Γ' → Set
         ; STy = λ A' → Tm ∙ (sty A') → Set
         ; Ty = λ A' → Tm ∙ A' → Set
         ; Sub = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
         ; Tm = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
         ; ∙ = λ _ → ↑p 𝟙
         ; _▹_ = λ Γ A σ' → Γ (p ∘ σ') × A (q [ σ' ])
         ; Nat = λ n → ↑p (⌜ eval n ⌝ ≡ n)
         ; Bool = λ b → ↑p (⌜ eval b ⌝ ≡ b)
         ; sty = idf
         ; _⇒_ = λ A B t' → ∀ {u'} → A u' → B (t' $ u')
         ; _∘_ = λ σ δ x → σ (δ x)
         ; id = idf
         ; ε = λ _ → *↑
         ; _,_ = λ σ t x → σ x ,Σ t x
         ; p = π₁
         ; q = π₂
         ; _[_] = λ t σ x → t (σ x)
         ; lam = λ t x y → t (x ,Σ y)
         ; app = λ t xy → t (π₁ xy) (π₂ xy)
         ; zero = λ _ → refl↑
         ; suc = λ n x → ↑[ ap suc ↓[ n x ]↓ ]↑
         ; isZero = λ n x → ↑[ isZero-⌜⌝
                             ◾ ap isZero ↓[ n x ]↓ ]↑
         ; _+_ = λ {_ m'} m n {ν'} x →
                   ↑[ +-⌜⌝ {eval (m' [ ν' ])}
                    ◾ ap2 _+_ ↓[ m x ]↓ ↓[ n x ]↓ ]↑
         ; true = λ _ → refl↑
         ; false = λ _ → refl↑
         -- ; ite = λ {_ _ b' u' v' _ A} b u v {ν'} x →
         --           let P t = A (ite t (u' [ ν' ]) (v' [ ν' ]))
         --           in  ind𝟚 (λ b → ⌜ b ⌝ ≡ b' [ ν' ] → P (b' [ ν' ]))
         --                    (λ e → transport P e (v x))
         --                    (λ e → transport P e (u x))
         --                    (eval (b' [ ν' ]))
         --                    ↓[ b x ]↓
         ; ite = λ {_ _ b' u' v' _ A} b u v {ν'} x →
                   P-ite {P = A} ↓[ b x ]↓ (u x) (v x)
         ; ass = refl
         ; idl = refl
         ; idr = refl
         ; ∙η = refl
         ; ▹β₁ = refl
         ; ▹β₂ = refl
         ; ▹η = refl
         ; [id] = refl
         ; [∘] = refl
         ; ⇒β = refl
         ; ⇒η = refl
         ; lam[] = refl
         ; zero[] = refl
         ; suc[] = refl
         ; isZero[] = refl
         ; +[] = refl
         ; true[] = refl
         ; false[] = refl
         ; ite[] = refl
         ; isZeroβ₁ = refl
         ; isZeroβ₂ = refl
         ; +β₁ = refl
         ; +β₂ = refl
         ; iteβ₁ = refl
         ; iteβ₂ = refl
         }
module Comp = DepAlgebra Comp

compB : {b : Tm ∙ (sty Bool)} → ⌜ eval b ⌝ ≡ b
compB {b} = ↓[ Comp.⟦ b ⟧t {id} *↑ ]↓

compN : {n : Tm ∙ (sty Nat)} → ⌜ eval n ⌝ ≡ n
compN {n} = ↓[ Comp.⟦ n ⟧t {id} *↑ ]↓

completeness : {A : STy} → {t : Tm ∙ (sty A)} → ⌜ eval t ⌝ ≡ t
completeness {Nat} {t} = compN
completeness {Bool} {t} = compB
\end{code}
