\chapter{Untyped lambda-calculus}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
\end{code}
\begin{code}
module ULC.Algebra where

open import Lib hiding (_∘_ ; _,_)

module I where
  data Con : Set where
    ∙ : Con
    _▹ : Con → Con 
  
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  postulate
    Sub : Con → Con → Set
    Tm : Con → Set

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ} → Sub Γ Δ → Tm Γ → Sub Γ (Δ ▹)
    p : ∀ {Γ} → Sub (Γ ▹) Γ

    q : ∀ {Γ} → Tm (Γ ▹)
    _[_] : ∀ {Γ Δ} → Tm Δ → Sub Γ Δ → Tm Γ
    lam : ∀ {Γ} → Tm (Γ ▹) → Tm Γ
    app : ∀ {Γ} → Tm Γ → Tm (Γ ▹)

    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ} {σ : Sub Γ Δ}{t : Tm Γ} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ} {σ : Sub Γ Δ}{t : Tm Γ} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ} {σ : Sub Γ (Δ ▹)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ} {t : Tm Γ} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ} {t : Tm Θ}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ⇒β : ∀ {Γ} {t : Tm (Γ ▹)} → app (lam t) ≡ t
    ⇒η : ∀ {Γ} {t : Tm Γ} → lam (app t) ≡ t
    
    lam[] : ∀ {Γ Δ} {t : Tm (Δ ▹)}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

  def : ∀ {Γ} → Tm Γ → Tm (Γ ▹) → Tm Γ
  def t u = u [ id , t ]

  _$_ : ∀ {Γ} → Tm Γ → Tm Γ → Tm Γ
  t $ u = def u (app t)

  ▹η' : ∀ {Γ} → p {Γ} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ} {σ : Sub Δ Θ}{t : Tm Δ}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ} {t : Tm Δ}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE ⇒β ⇒η #-}
  {-# REWRITE lam[] app[] #-}

record Algebra {i j k} : Set (lsuc (i ⊔ j ⊔ k)) where
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_

  field
    Con : Set i
    Sub : Con → Con → Set j
    Tm : Con → Set k

    ∙ : Con
    _▹ : Con → Con 

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ} → Sub Γ Δ → Tm Γ → Sub Γ (Δ ▹)
    p : ∀ {Γ} → Sub (Γ ▹) Γ

    q : ∀ {Γ} → Tm (Γ ▹)
    _[_] : ∀ {Γ Δ} → Tm Δ → Sub Γ Δ → Tm Γ
    lam : ∀ {Γ} → Tm (Γ ▹) → Tm Γ
    app : ∀ {Γ} → Tm Γ → Tm (Γ ▹)

    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ} {σ : Sub Γ Δ}{t : Tm Γ} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ} {σ : Sub Γ Δ}{t : Tm Γ} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ} {σ : Sub Γ (Δ ▹)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ} {t : Tm Γ} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ} {t : Tm Θ}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ⇒β : ∀ {Γ} {t : Tm (Γ ▹)} → app (lam t) ≡ t
    ⇒η : ∀ {Γ} {t : Tm Γ} → lam (app t) ≡ t
    
    lam[] : ∀ {Γ Δ} {t : Tm (Δ ▹)}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

  def : ∀ {Γ} → Tm Γ → Tm (Γ ▹) → Tm Γ
  def t u = u [ id , t ]

  _$_ : ∀ {Γ} → Tm Γ → Tm Γ → Tm Γ
  t $ u = def u (app t)

  ▹η' : ∀ {Γ} → p {Γ} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ} {σ : Sub Δ Θ}{t : Tm Δ}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  def⇒ : ∀ {Γ} {t : Tm Γ}{u : Tm (Γ ▹)} → def t u ≡ lam u $ t
  def⇒ = ap (def _) ⇒β ⁻¹

  app[] : ∀ {Γ Δ} {t : Tm Δ}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  def[] : ∀ {Γ Δ} {t : Tm Δ}{u : Tm (Δ ▹)}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▹β₁
            ◾ idr)
          ▹β₂))
    refl

  $[] : ∀ {Γ Δ} {t : Tm Δ}{u : Tm Δ}{σ : Sub Γ Δ} →
    (t $ u) [ σ ] ≡ t [ σ ] $ u [ σ ]
  $[] = def[] ◾ ap (def _) app[] ⁻¹

  ⇒η' : ∀ {Γ} {t : Tm Γ} → lam (t [ p ] $ q) ≡ t
  ⇒η' {t = t} = ap lam
                ( ap (_[ id , q ]) app[]
                ◾ [∘]
                ◾ ap ((app t) [_])
                  ( ,∘
                  ◾ ap2 _,_
                    ( ass
                    ◾ ap (p ∘_) ▹β₁)
                    ( ▹β₂
                    ◾ [id] ⁻¹)
                  ◾ ▹η)
                ◾ [id])
              ◾ ⇒η

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ ⟧C = ⟦ Γ ⟧C ▹

  postulate
    ⟦_⟧S : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ} → I.Tm Γ → Tm ⟦ Γ ⟧C
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ} {σ : I.Sub Γ Δ}{t : I.Tm Γ} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ} → ⟦ I.p {Γ} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ} → ⟦ I.q {Γ} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ} {t : I.Tm Δ}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    ⟦lam⟧ : ∀ {Γ} {t : I.Tm (Γ I.▹)} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ} {t : I.Tm Γ} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦q⟧ ⟦[]⟧ ⟦lam⟧ ⟦app⟧ #-}
  
  ⟦def⟧ : ∀ {Γ} {t : I.Tm Γ}{u : I.Tm (Γ I.▹)} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ} {t : I.Tm Γ}{u : I.Tm Γ} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl

record DepAlgebra {i j k} : Set (lsuc (i ⊔ j ⊔ k)) where
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Con : I.Con → Set i
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set j
    Tm : ∀ {Γ'} → Con Γ' → I.Tm Γ' → Set k

    ∙ : Con I.∙
    _▹ : ∀ {Γ'} → Con Γ' → Con (Γ' I.▹)

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    _,_ : ∀ {Γ' Δ' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'} →
      Sub Γ Δ σ' → Tm Γ t' → Sub Γ (Δ ▹) (σ' I., t')
    p : ∀ {Γ'} {Γ : Con Γ'} → Sub (Γ ▹) Γ I.p

    q : ∀ {Γ'} {Γ : Con Γ'} → Tm (Γ ▹) I.q
    _[_] : ∀ {Γ' Δ' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'} →
      Tm Δ t' → Sub Γ Δ σ' → Tm Γ (t' I.[ σ' ])
    lam : ∀ {Γ' t'} {Γ : Con Γ'} →
      Tm (Γ ▹) t' → Tm Γ (I.lam t')
    app : ∀ {Γ' t'} {Γ : Con Γ'} →
      Tm Γ t' → Tm (Γ ▹) (I.app t')

    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ ap (Sub Γ ∙) I.∙η ]= ε

    ▹β₁ : ∀ {Γ' Δ' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}
      {σ : Sub Γ (Δ ▹) σ'} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ' t'} {Γ : Con Γ'}{t : Tm Γ t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}
      {t : Tm Θ t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ⇒β : ∀ {Γ' t'} {Γ : Con Γ'}
      {t : Tm (Γ ▹) t'} → app (lam t) ≡ t
    ⇒η : ∀ {Γ' t'} {Γ : Con Γ'}
      {t : Tm Γ t'} → lam (app t) ≡ t
    
    lam[] : ∀ {Γ' Δ' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}
      {t : Tm (Δ ▹) t'}{σ : Sub Γ Δ σ'} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

  def : ∀ {Γ' t' u'}{Γ : Con Γ'} →
    Tm Γ t' → Tm (Γ ▹) u' → Tm Γ (I.def t' u')
  def t u = u [ id , t ]

  _$_ : ∀ {Γ' t' u'}{Γ : Con Γ'} →
    Tm Γ t' → Tm Γ u' → Tm Γ (t' I.$ u')
  t $ u = def u (app t)

  -------------------------------------------
  -- eliminator
  -------------------------------------------

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ ⟧C = ⟦ Γ ⟧C ▹

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ} (t : I.Tm Γ) → Tm ⟦ Γ ⟧C t
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ} {σ : I.Sub Γ Δ}{t : I.Tm Γ} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ} → ⟦ I.p {Γ} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

    ⟦q⟧ : ∀ {Γ} → ⟦ I.q {Γ} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ} {t : I.Tm Δ}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    ⟦lam⟧ : ∀ {Γ} {t : I.Tm (Γ I.▹)} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ} {t : I.Tm Γ} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦q⟧ ⟦[]⟧ ⟦lam⟧ ⟦app⟧ #-}

  ⟦def⟧ : ∀ {Γ}{t : I.Tm Γ}{u : I.Tm (Γ I.▹)} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ} {t : I.Tm Γ}{u : I.Tm Γ} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl
\end{code}
