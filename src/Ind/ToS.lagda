\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Ind.ToS where

open import Lib renaming (_×_ to _⊗_)
\end{code}
\begin{code}
module J where
  data Ar : Set
  data Sign : Set
  data Ty : Set

  data Ar where
    X : Ar
    X⇒_ : Ar → Ar
    _⇛_ : Ty → Ar → Ar

  data Sign where
    ◆ : Sign
    _▷_ : Sign → Ar → Sign

  data Ty where
    Unit : Ty
    Empty : Ty
    Nat : Ty
    _⇒_ : Ty → Ty → Ty
    _×_ : Ty → Ty → Ty
    _+_ : Ty → Ty → Ty
    ind : Sign → Ty

data Ty : Set where
  Unit : Ty
  Empty : Ty
  Nat : Ty
  _⇒_ : Ty → Ty → Ty
  _×_ : Ty → Ty → Ty
  _+_ : Ty → Ty → Ty

⟦_⟧T : Ty → Set
⟦ Unit ⟧T = ↑p 𝟙
⟦ Empty ⟧T = ↑p 𝟘
⟦ Nat ⟧T = ℕ
⟦ A ⇒ B ⟧T = ⟦ A ⟧T → ⟦ B ⟧T
⟦ A × B ⟧T = ⟦ A ⟧T ⊗ ⟦ B ⟧T
⟦ A + B ⟧T = ⟦ A ⟧T ⊎ ⟦ B ⟧T

module I where
  infixr 5 X⇒_
  infixr 5 _⇛_
  infixl 5 _▷_

  data Ar : Set where
    X : Ar
    X⇒_ : Ar → Ar
    _⇛_ : Ty → Ar → Ar

  data Sign : Set where
    ∙ : Sign
    _▷_ : Sign → Ar → Sign

record Algebra {i j} : Set (lsuc (i ⊔ j)) where
  infixr 5 X⇒_
  infixr 5 _⇛_
  infixl 5 _▷_

  field
    Ar : Set i
    X : Ar
    X⇒_ : Ar → Ar
    _⇛_ : Ty → Ar → Ar

    Sign : Set j
    ∙ : Sign
    _▷_ : Sign → Ar → Sign

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧A : I.Ar → Ar
  ⟦ I.X ⟧A = X
  ⟦ I.X⇒ R ⟧A = X⇒ ⟦ R ⟧A
  ⟦ A I.⇛ R ⟧A = A ⇛ ⟦ R ⟧A

  ⟦_⟧S : I.Sign → Sign
  ⟦ I.∙ ⟧S = ∙
  ⟦ Ω I.▷ R ⟧S = ⟦ Ω ⟧S ▷ ⟦ R ⟧A

St : Algebra
St = record
       { Ar = Set → Set
       ; X = idf
       ; X⇒_ = λ A C → C → A C
       ; _⇛_ = λ T A C → ⟦ T ⟧T → A C
       ; Sign = Set → Set
       ; ∙ = λ _ → ↑p 𝟙
       ; _▷_ = λ Ω A C → Ω C ⊗ A C
       }
open Algebra St using (⟦_⟧A ; ⟦_⟧S)
open I
{-
_ᴬ : Sign → Set → Set
_ᴬ = ⟦_⟧S
-}
N : Sign
N = ∙ ▷ X ▷ (X⇒ X)
{-
Nᴬ : ∀ {C} → (N ᴬ) C ≡ (↑p 𝟙 ⊗ C ⊗ (C → C))
Nᴬ = refl

L : Ty → Sign
L A = ∙ ▷ X ▷ (A ⇛ X⇒ X)

Lᴬ : ∀ {C} → ((L Nat) ᴬ) C ≡ (↑p 𝟙 ⊗ C ⊗ (ℕ → C → C))
Lᴬ = refl
-}

_ᴬA : Ar → Set → Set
(X ᴬA) A = A
((X⇒ R) ᴬA) A = A → (R ᴬA) A
((B ⇛ R) ᴬA) A = ⟦ B ⟧T → (R ᴬA) A

_ᴬS : Sign → Set → Set
(∙ ᴬS)       A = ↑p 𝟙
((Ω ▷ R) ᴬS) A = (Ω ᴬS) A ⊗ (R ᴬA) A

data Var : Sign → Ar → Set where
  vz : ∀{Ω R} → Var (Ω ▷ R) R
  vs : ∀{Ω R R'} → Var Ω R → Var (Ω ▷ R') R

data Tm : Sign → Ar → Set where
  var : ∀{Ω R} → Var Ω R → Tm Ω R
  app : ∀{Ω R} → Tm Ω (X⇒ R) → Tm Ω X → Tm Ω R
  app̂ : ∀{Ω T R} → Tm Ω (T ⇛ R) → ⟦ T ⟧T → Tm Ω R

-- ind Ω : Ty
-- ind Ω standard interpretacioja
ind : Sign → Set
ind Ω = Tm Ω X

-- Ω ᴬS : Set → Set
-- Ω ᴬ' : Ty  → Ty
-- con Ω : Tm ∙ ((Ω ᴬ') (ind Ω))
-- con Ω standard interpretacioja
con : (Ω : Sign) → (Ω ᴬS) (ind Ω)
con Ω = {!!}

recVar : (Ω : Sign) → (A : Set) → (Ω ᴬS) A → {R : Ar} → Var Ω R → (R ᴬA) A
recVar (Ω ▷ R) A f vz = π₂ f
recVar (Ω ▷ R) A f (vs c) = recVar Ω A (π₁ f) c

rec' : (Ω : Sign)(A : Set)(f : (Ω ᴬS) A){R : Ar} → Tm Ω R → (R ᴬA) A
rec' Ω A f (var x) = recVar Ω A f x
rec' Ω A f (app t t₁) = rec' Ω A f t (rec' Ω A f t₁)
rec' Ω A f (app̂ t x) = rec' Ω A f t x

-- rec Ω : ∀{A} → Tm (∙ ▷ Ω ᴬ' A ▷ ind Ω) A
-- rec Ω : ∀{A} → Tm ∙ (Ω ᴬ' A ⇒ ind Ω ⇒ A)
-- rec Ω standard interpretacioja
rec : (Ω : Sign) → (A : Set) → (Ω ᴬS) A → ind Ω → A
rec Ω A f t = rec' Ω A f t

-- natural numbers example

zero : ind N
zero = var (vs vz)

suc : ind N → ind N
suc = λ n → app (var vz) n

Nβ₁ : ∀{A}{f : (N ᴬS) A} → rec N A f zero ≡ π₂ (π₁ f)
Nβ₁ = refl

Nβ₂ : ∀{A}{f : (N ᴬS) A}{n : ind N} → rec N A f (suc n) ≡ π₂ f (rec N A f n)
Nβ₂ = refl
\end{code}
