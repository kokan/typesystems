\begin{code}[hide]
{-# OPTIONS --prop --rewriting --type-in-type #-}

module Ind.Standard where

open import Lib renaming (_∘_ to _∘f_ ; _×_ to _×m_ ; _,_ to _,Σ_)
open import Ind.Algebra
\end{code}
\begin{code}
module M where
  data Ar : Set where
    X : Ar
    X⇒_ : Ar → Ar
    _⇛_ : Set → Ar → Ar

  data Sign : Set where
    ◆ : Sign
    _▷_ : Sign → Ar → Sign
    
  infixr 6 X⇒_
  infixr 6 _⇛_
  infixl 5 _▷_

  _ᴬA : Ar → Set → Set
  (X ᴬA) C = C
  ((X⇒ R) ᴬA) C = C → (R ᴬA) C
  ((A ⇛ R) ᴬA) C = A → (R ᴬA) C

  _ᴬ : Sign → Set → Set
  (◆ ᴬ) C = ↑p 𝟙
  ((Ω ▷ R) ᴬ) C = (Ω ᴬ) C ×m (R ᴬA) C

  data VarI : Sign → Ar → Set where
    vz : ∀ {Ω R} → VarI (Ω ▷ R) R
    vs : ∀ {Ω R Q} → VarI Ω R → VarI (Ω ▷ Q) R

  data TmI : Sign → Ar → Set where
    var : ∀ {Ω R} → VarI Ω R → TmI Ω R
    _$_ : ∀ {Ω R} → TmI Ω (X⇒ R) → TmI Ω X → TmI Ω R
    _$̂_ : ∀ {Ω A R} → TmI Ω (A ⇛ R) → A → TmI Ω R

  infixl 6 _$_
  infixl 6 _$̂_

  ind : Sign → Set
  ind Ω = TmI Ω X

  _ⱽ : Sign → Sign → Set
  (◆ ⱽ) Ω' = ↑p 𝟙
  ((Ω ▷ R) ⱽ) Ω' = (Ω ⱽ) Ω' ×m VarI Ω' R

  shift : ∀ {Ω Ω' Q} → (Ω ⱽ) Ω' → (Ω ⱽ) (Ω' ▷ Q)
  shift {◆} .*↑ = *↑
  shift {Ω ▷ R} (α ,Σ v) = shift α ,Σ vs v

  conVar : ∀ Ω → (Ω ⱽ) Ω
  conVar ◆ = *↑
  conVar (Ω ▷ R) = shift (conVar Ω) ,Σ vz

  conA : ∀ {R Ω'} → TmI Ω' R → (R ᴬA) (ind Ω')
  conA {X} t = t
  conA {X⇒ R} f = λ u → conA (f $ u)
  conA {A ⇛ R} f = λ a → conA (f $̂ a)

  con' : ∀ {Ω Ω'}→ (Ω ⱽ) Ω' → (Ω ᴬ) (ind Ω')
  con' {◆} .*↑ = *↑
  con' {Ω ▷ R} (α ,Σ v) = con' α ,Σ conA (var v)

  con : ∀ Ω → (Ω ᴬ) (ind Ω)
  con Ω = con' (conVar Ω)

  recVar : ∀ {A R Ω} → (Ω ᴬ) A → VarI Ω R → (R ᴬA) A
  recVar (ω ,Σ c) vz = c
  recVar (ω ,Σ c) (vs v) = recVar ω v

  rec' : ∀ {A R Ω} → (Ω ᴬ) A → TmI Ω R → (R ᴬA) A
  rec' ω (var v) = recVar ω v
  rec' ω (f $ u) = rec' ω f (rec' ω u)
  rec' ω (f $̂ a) = rec' ω f a

  rec : ∀ {A} Ω → (Ω ᴬ) A → ind Ω → A
  rec Ω ω t = rec' ω t
{-
  _ᴹA : ∀ {A B} R → (R ᴬA) A → (R ᴬA) B → (A → B) → Prop
  (X ᴹA) x y F = F x ≡ y
  ((X⇒ R) ᴹA) f g F = ∀ {x} → (R ᴹA) (f x) (g (F x)) F
  ((C ⇛ R) ᴹA) f g F = ∀ {c} → (R ᴹA) (f c) (g c) F

  _ᴹ : ∀ {A B} Ω → (Ω ᴬ) A → (Ω ᴬ) B → (A → B) → Prop
  (◆ ᴹ) .*↑ .*↑ F = 𝟙
  ((Ω ▷ R) ᴹ) (γ ,Σ x) (δ ,Σ y) F = (Ω ᴹ) γ δ F ×p (R ᴹA) x y F

  N : Sign
  N = ◆ ▷ X ▷ X⇒ X

  ωN : (N ᴬ) ℕ
  ωN = *↑ ,Σ O ,Σ S

  βN : (ℕ → ℕ) → Prop
  βN = (N ᴹ) ωN ωN

  T : Sign
  T = ◆ ▷ ind N ⇛ X ▷ X⇒ X⇒ X

  ⌜_⌝N : ℕ → ind N
  ⌜ O ⌝N = var (vs vz)
  ⌜ S n ⌝N = var vz $ ⌜ n ⌝N

  evalN : ind N → ℕ
  evalN = rec N ωN

  data Tree : Set where
    lf : ℕ → Tree
    nd : Tree → Tree → Tree

  ωT : (T ᴬ) Tree
  ωT = *↑ ,Σ lf ∘f evalN ,Σ nd

  βT : (Tree → Tree) → Prop
  βT = (T ᴹ) ωT ωT

  zero : ind N
  zero = π₂ (π₁ (con N))

  suc : ind N → ind N
  suc = π₂ (con N)

  leaf : ind N → ind T
  leaf = π₂ (π₁ (con T))

  node : ind T → ind T → ind T
  node = π₂ (con T)

  Nβ₁ : ∀ {A} {ω : (N ᴬ) A} → rec N ω zero ≡ π₂ (π₁ ω)
  Nβ₁ = refl

  Nβ₂ : ∀ {A} {ω : (N ᴬ) A}{n : ind N} → rec N ω (suc n) ≡ π₂ ω (rec N ω n)
  Nβ₂ = refl

  Nβ : ∀ {A} {ω : (N ᴬ) A} → (N ᴹ) (con N) ω (rec N ω)
  Nβ = * ,p refl ,p refl

  Tβ : ∀ {A} {ω : (T ᴬ) A} → (T ᴹ) (con T) ω (rec T ω)
  Tβ = * ,p refl ,p refl
-}

  _ᴹA : ∀ {Γ : Set} {A B} R → (Γ → (R ᴬA) A) → (Γ → (R ᴬA) B) →
    ((Γ → A) → (Γ → B)) → Prop
  (X ᴹA) t u F = F t ≡ u
  ((X⇒ R) ᴹA) f g F = ∀ {t} → (R ᴹA) (f ∘' t) (g ∘' F t) F
  ((C ⇛ R) ᴹA) f g F = ∀ {t} → (R ᴹA) (f ∘' t) (g ∘' t) F

  _ᴹ : ∀ {Γ : Set} {A B} Ω → (Γ → (Ω ᴬ) A) → (Γ → (Ω ᴬ) B) →
    ((Γ → A) → (Γ → B)) → Prop
  (◆ ᴹ) t u F = 𝟙'
  ((Ω ▷ R) ᴹ) t u F =
    (Ω ᴹ) (π₁ ∘f t) (π₁ ∘f u) F ×p (R ᴹA) (π₂ ∘f t) (π₂ ∘f u) F

  postulate
    indβ : ∀ {Γ : Set} {A Ω} {ω : Γ → (Ω ᴬ) A} →
      (Ω ᴹ) (λ _ → con Ω) ω (λ t γ → rec Ω (ω γ) (t γ))

  N : Sign
  N = ◆ ▷ X ▷ X⇒ X

  Nat : Set
  Nat = ind N

  zero : Nat
  zero = π₂ (π₁ (con N))

  suc : Nat → Nat
  suc = π₂ (con N)

  Nβ : {Γ A : Set} {ω : Γ → (N ᴬ) A} →
    (N ᴹ) (λ _ → con N) ω (λ t γ → rec N (ω γ) (t γ))
  Nβ = *' ,p refl ,p refl

St : Algebra
St = record
       { Ar = M.Ar
       ; Sign = M.Sign
       ; Ty = Set
       ; Con = Set
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; X = M.X
       ; X⇒_ = M.X⇒_
       ; _⇛_ = M._⇛_
       ; ◆ = M.◆
       ; _▷_ = M._▷_
       ; Unit = ↑p 𝟙
       ; Empty = ↑p 𝟘
       ; _⇒_ = λ A B → A → B
       ; _×_ = _×m_
       ; _+_ = _⊎_
       ; ind = M.ind
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×m_
       ; _ᴬA = M._ᴬA
       ; _ᴬ = M._ᴬ
       ; _ᴹA = M._ᴹA
       ; _ᴹ = M._ᴹ
       ; _∘_ = _∘f_
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ,Σ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; lam = λ t γ a → t (γ ,Σ a)
       ; app = λ t γa → t (π₁ γa) (π₂ γa)
       ; tt = const *↑
       ; ⟨_,_⟩ = λ u v γ → u γ ,Σ v γ
       ; proj₁ = λ t γ → π₁ (t γ)
       ; proj₂ = λ t γ → π₂ (t γ)
       ; absurd = λ t γ → ⟦ ↓[ t γ ]↓ ⟧𝟘
       ; inl = λ t γ → ι₁ (t γ)
       ; inr = λ t γ → ι₂ (t γ)
       ; case = λ t u v γ →
                  case⊎ (t γ) (λ a → u (γ ,Σ a)) (λ b → v (γ ,Σ b))
       ; con = λ Ω _ → M.con Ω
       ; rec = λ Ω u t γ → M.rec Ω (u γ) (t γ)
       ; ᴬAX = refl
       ; ᴬAX⇒ = refl
       ; ᴬA⇛ = refl
       ; ᴬ◆ = refl
       ; ᴬ▷ = refl
       ; ᴹAX = refl
       ; ᴹAX⇒ = refl
       ; ᴹA⇛ = refl
       ; ᴹ◆ = refl
       ; ᴹ▷ = refl
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; uη = refl
       ; ×β₁ = refl
       ; ×β₂ = refl
       ; ×η = refl
       ; +β₁ = refl
       ; +β₂ = refl
       ; indβ = M.indβ
       ; lam[] = refl
       ; tt[] = refl
       ; ⟨,⟩[] = refl
       ; absurd[] = refl
       ; inl[] = refl
       ; inr[] = refl
       ; case[] = refl
       ; con[] = refl
       ; rec[] = refl
       }
module St = Algebra St
\end{code}
