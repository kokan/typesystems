\appendix

\chapter{Metatheory}

We write \verb$:$ instead of \verb$∈$, \verb$(x : A) → B$ instead of \verb$∀x.B$ (where \verb$x$
quantifies over \verb$A$), \verb$Σ A λ x → B$ instead of \verb$∃x.B$, \verb$_,_$
constructor. \verb$⊎$ with constructors \verb$ι₁$ and \verb$ι₂$ and pattern
matching. \verb$Set$, \verb$Prop$, \verb$→$, application is space, curried
functions. \verb$𝟙$ with constructor \verb$*$. \verb$𝟚$ with constructors
\verb$O$, \verb$I$. \verb$ℕ$ with constructors \verb$O$, \verb$S$. What
are relations and predicates, proof-relevant relations. \verb$Prop$ is a
subtype of \verb$Set$, \verb$↑p$.

Inductive sets. For small definitions, we use pattern matching.

For closed inductive sets, equality can be decided, we
denote this by \verb$≟$.

\begin{code}
{-# OPTIONS --prop --rewriting #-}

module Lib where

open import Agda.Primitive public

idf : ∀ {i} {A : Set i} → A → A
idf x = x

const : ∀ {i j} {A : Set i}{B : Set j} → A → B → A
const x y = x

infixl 6 _⊙_
_⊙_ : ∀ {i j k} {A : Set i}{B : A → Set j}{C : ∀ {x} → B x → Set k}
  (f : ∀ {x} (y : B x) → C y)(g : (x : A) → B x)
  (x : A) → C (g x)
(f ⊙ g) x = f (g x)

infixl 6 _∘_
_∘_ : ∀ {i j k} {A : Set i}{B : Set j}{C : Set k} →
  (B → C) → (A → B) → A → C
(f ∘ g) x = f (g x)

infixl 6 _∘'_
_∘'_ : ∀ {i j k} {A : Set i}{B : Set j}{C : Set k} →
  (A → B → C) → (A → B) → A → C
(f ∘' g) x = f x (g x)

data 𝟘 : Prop where

⟦_⟧𝟘 : ∀ {i} {A : Set i} → 𝟘 → A
⟦ () ⟧𝟘

⟦_⟧𝟘p : ∀ {i} {A : Prop i} → 𝟘 → A
⟦ () ⟧𝟘p

record 𝟙 : Prop where
  constructor *
open 𝟙 public

record 𝟙' {i} : Prop i where
  constructor *'
open 𝟙' public

data 𝟚 : Set where
  O : 𝟚
  I : 𝟚

ind𝟚 : ∀ {i} (P : 𝟚 → Set i) →
  P O → P I →
  (b : 𝟚) → P b
ind𝟚 P u v O = u
ind𝟚 P u v I = v

ind𝟚p : ∀ {i} (P : 𝟚 → Prop i) →
  P O → P I →
  (b : 𝟚) → P b
ind𝟚p P u v O = u
ind𝟚p P u v I = v

if_then_else_ : ∀ {i} {A : Set i} → 𝟚 → A → A → A
if b then u else v = ind𝟚 _ v u b

infix 6 _≟𝟚_
_≟𝟚_ : 𝟚 → 𝟚 → 𝟚
O ≟𝟚 O = I
O ≟𝟚 I = O
I ≟𝟚 O = O
I ≟𝟚 I = I

and𝟚 : 𝟚 → 𝟚 → 𝟚
and𝟚 O b = O
and𝟚 I b = b

or𝟚 : 𝟚 → 𝟚 → 𝟚
or𝟚 O b = b
or𝟚 I b = I

data ℕ : Set where
  O : ℕ
  S : ℕ → ℕ
{-# BUILTIN NATURAL ℕ #-}

rec-ℕ : {A : Set} → A → (A → A) → ℕ → A
rec-ℕ z s O = z
rec-ℕ z s (S n) = s (rec-ℕ z s n)

indℕ : ∀ {i} (P : ℕ → Set i) →
  P O → ({n : ℕ} → P n → P (S n)) →
  (n : ℕ) → P n
indℕ P u v O = u
indℕ P u v (S n) = v (indℕ P u v n)

indℕp : ∀ {i} (P : ℕ → Prop i) →
  P O → ({n : ℕ} → P n → P (S n)) →
  (n : ℕ) → P n
indℕp P u v O = u
indℕp P u v (S n) = v (indℕp P u v n)

max : ℕ → ℕ → ℕ
max O     b = b
max (S a) O = S a
max (S a) (S b) = S (max a b)

infix 4 _≤_
data _≤_ : ℕ → ℕ → Prop where
  z≤n : ∀ {n} → O ≤ n
  s≤s : ∀ {m n} (m≤n : m ≤ n) → S m ≤ S n

-- O ≤ b = 𝟙
-- S a ≤ O = 𝟘
-- S a ≤ S b = a ≤ b

infix 6 _≟ℕ_
_≟ℕ_ : ℕ → ℕ → 𝟚
O ≟ℕ O = I
O ≟ℕ S b = O
S a ≟ℕ O = O
S a ≟ℕ S b = a ≟ℕ b

infixl 7 _+ℕ_
_+ℕ_ : ℕ → ℕ → ℕ
O +ℕ b = b
S a +ℕ b = S (a +ℕ b)

infixl 8 _*ℕ_
_*ℕ_ : ℕ → ℕ → ℕ
O *ℕ b = 0
S a *ℕ b = b +ℕ a *ℕ b

infix 9 _^ℕ_
_^ℕ_ : ℕ → ℕ → ℕ
a ^ℕ O = 1
a ^ℕ S b = a *ℕ a ^ℕ b

infixr 5 _∷_
data ⁅_⁆ {a}(A : Set a) : Set a where
  ⁅⁆  : ⁅ A ⁆
  _∷_ : (x : A) (xs : ⁅ A ⁆) → ⁅ A ⁆

rec-⁅⁆ : {A B : Set} → B → (B → A → B) → ⁅ A ⁆ → B
rec-⁅⁆ e f ⁅⁆ = e
rec-⁅⁆ e f (l ∷ ls) = f (rec-⁅⁆ e f ls) l

infixr 5 _::_
data Vec {i} (A : Set i) : ℕ → Set i where
  [] : Vec A O
  _::_ : ∀ {n} → A → Vec A n → Vec A (S n)

data 𝕋 : Set where
  Leaf : 𝕋
  Node : 𝕋 → 𝕋 → 𝕋

rec-𝕋 : {A : Set} → A → (A → A → A) → 𝕋 → A
rec-𝕋 l n Leaf = l
rec-𝕋 l n (Node ll rr) = n (rec-𝕋 l n ll) (rec-𝕋 l n rr)

data 𝕋1 (A : Set) : Set where
  Leaf : 𝕋1 A
  Node : 𝕋1 A → A → 𝕋1 A → 𝕋1 A

rec-𝕋1 : {A B : Set} → B → (B → A → B → B) → 𝕋1 A → B
rec-𝕋1 l n Leaf = l
rec-𝕋1 l n (Node ll a rr) = n (rec-𝕋1 l n ll) a (rec-𝕋1 l n rr)

data 𝕋2 (A : Set) (B : Set) : Set where
  Leaf : B → 𝕋2 A B
  Node : 𝕋2 A B → A → 𝕋2 A B → 𝕋2 A B

rec-𝕋2 : {A B C : Set} → (B → C) → (C → A → C → C) → 𝕋2 A B → C
rec-𝕋2 l n (Leaf b) = l b
rec-𝕋2 l n (Node ll a rr) = n (rec-𝕋2 l n ll) a (rec-𝕋2 l n rr)

data 𝕋3 (A : Set) : Set where
  Leaf : A → 𝕋3 A
  Node : 𝕋3 A → 𝕋3 A → 𝕋3 A

rec-𝕋3 : {A B : Set} → (A → B) → (B → B → B) → 𝕋3 A → B
rec-𝕋3 l n (Leaf a) = l a
rec-𝕋3 l n (Node ll rr) = n (rec-𝕋3 l n ll) (rec-𝕋3 l n rr)

infixl 4 _,_
record Σ {i j} (A : Set i)(B : A → Set j) : Set (i ⊔ j) where
  constructor _,_
  field
    π₁ : A
    π₂ : B π₁
open Σ public

infixl 4 _,p_
record Σp {i j} (A : Prop i) (B : A → Prop j) : Prop (i ⊔ j) where
  constructor _,p_
  field
    π₁ : A
    π₂ : B π₁
open Σp public

infixr 1 _⊎_
data _⊎_ {i j} (A : Set i)(B : Set j) : Set (i ⊔ j) where
  ι₁ : A → A ⊎ B
  ι₂ : B → A ⊎ B

infixr 1 _⊎p_
data _⊎p_ {i j} (A : Prop i)(B : Prop j) : Prop (i ⊔ j) where
  ι₁ : A → A ⊎p B
  ι₂ : B → A ⊎p B

ind⊎ : ∀ {i j k} {A : Set i}{B : Set j}(P : A ⊎ B → Set k) →
  ((a : A) → P (ι₁ a)) → ((b : B) → P (ι₂ b)) →
  (t : A ⊎ B) → P t
ind⊎ P u v (ι₁ t) = u t
ind⊎ P u v (ι₂ t) = v t

case⊎ : ∀ {i j k} {A : Set i}{B : Set j}{C : Set k} →
  A ⊎ B → (A → C) → (B → C) → C
case⊎ (ι₁ t) u v = u t
case⊎ (ι₂ t) u v = v t

record ↑p {i} (A : Prop i) : Set i where
  constructor ↑[_]↑
  field
    ↓[_]↓ : A
open ↑p public

*↑ : ↑p 𝟙
*↑ = ↑[ * ]↑

infix 3 ¬_
¬_ : ∀ {i} → Prop i → Prop i
¬ A = A → 𝟘

infixl 2 _×_
_×_ : ∀ {i j} → Set i → Set j → Set (i ⊔ j)
A × B = Σ A (const B)

rec-× : {A B C : Set} → (A × B → C) → (A × B) → C
rec-× f ab = f ab
-- rec-× = _$_

infixl 2 _×p_
_×p_ : ∀ {i j} → Prop i → Prop j → Prop (i ⊔ j)
A ×p B = Σp A (λ _ → B)

infix 1 _↔_
_↔_ : ∀ {i j} → Set i → Set j → Set (i ⊔ j)
A ↔ B = (A → B) × (B → A)

infix 4 _≡_
data _≡_ {i} {A : Set i}(x : A) : A → Prop i where
  refl : x ≡ x
{-# BUILTIN REWRITE _≡_ #-}

refl↑ : ∀ {i} {A : Set i}{x : A} → ↑p (x ≡ x)
refl↑ = ↑[ refl ]↑

-- symmetry : ∀ {i} {A : Set i} {x y : A} → x ≡ y → y ≡ x
-- symmetry refl = refl

transportp : ∀ {i j} {A : Set i}(P : A → Prop j)
  {x y : A} → x ≡ y → P x → P y
transportp P refl p = p 

transportp2 : ∀ {i j k} {A : Set i}{B : Set j}(P : A → B → Prop k)
  {x y : A}{u v : B} → x ≡ y → u ≡ v → P x u → P y v
transportp2 P refl refl p = p

postulate
  transport : ∀ {i j} {A : Set i}(P : A → Set j)
    {x y : A} → x ≡ y → P x → P y
  transport-refl : ∀ {i j} {A : Set i}{P : A → Set j}
    {x : A}{px : P x} → transport P refl px ≡ px
  {-# REWRITE transport-refl #-}

cong : ∀ {i j} {A : Set i} {B : Set j} {x x' : A} →
  (f : A → B) → x ≡ x' → f x ≡ f x'
cong f refl = refl

congruency = cong

cong-2 : ∀ {i j k} {A : Set i} {B : Set j} {C : Set k} {x x' : A} {y y' : B} →
  (f : A → B → C) → x ≡ x' → y ≡ y' → f x y ≡ f x' y'
cong-2 f refl refl = refl

cong-3 : ∀ {i j k l} {A : Set i} {B : Set j} {C : Set k} {D : Set l} →
  {x x' : A} → {y y' : B} → {z z' : C} →
  (f : A → B → C → D) → x ≡ x' → y ≡ y' → z ≡ z' → f x y z ≡ f x' y' z'
cong-3 f refl refl refl = refl

infix 3 _∎
_∎ : ∀ {i} {A : Set i}(x : A) → x ≡ x
x ∎ = refl

-- symmetry
infix 5 _⁻¹
_⁻¹ : ∀ {i} {A : Set i}{x y : A} → x ≡ y → y ≡ x
refl ⁻¹ = refl

-- transitivity
infixr 4 _◾_
_◾_ : ∀ {i} {A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ◾ p = p

infixr 2 _≡⟨_⟩_
_≡⟨_⟩_ : ∀{ℓ}{A : Set ℓ}(x : A){y z : A} → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ x≡y ⟩ y≡z = x≡y ◾ y≡z

infixr 2 _≡≡_
_≡≡_ : ∀{ℓ}{A : Set ℓ}(x : A){y : A} → x ≡ y → x ≡ y
x ≡≡ x≡y = x≡y

ap : ∀ {i j} {A : Set i}{B : Set j}(f : A → B)
  {x y : A} → x ≡ y → f x ≡ f y
ap f refl = refl

ap2 : ∀ {i j k} {A : Set i}{B : Set j}{C : Set k}(f : A → B → C)
  {x₁ y₁ : A}{x₂ y₂ : B} → x₁ ≡ y₁ → x₂ ≡ y₂ → f x₁ x₂ ≡ f y₁ y₂
ap2 f refl refl = refl

ap3 : ∀ {i j k l} {A : Set i}{B : Set j}{C : Set k}{D : Set l}
  (f : A → B → C → D){x₁ y₁ : A}{x₂ y₂ : B}{x₃ y₃ : C} →
  x₁ ≡ y₁ → x₂ ≡ y₂ → x₃ ≡ y₃ → f x₁ x₂ x₃ ≡ f y₁ y₂ y₃
ap3 f refl refl refl = refl

rail : ∀ {i} {A : Set i}{x y z w : A} →
  x ≡ z → y ≡ w → z ≡ w → x ≡ y
rail t b r = t ◾ r ◾ b ⁻¹

coe : ∀ {i} {A B : Set i} → A ≡ B → A → B
coe = transport idf

infix 4 _=[_]=_
_=[_]=_ : ∀ {i}{A B : Set i} → A → A ≡ B → B → Prop i
x =[ p ]= y = coe p x ≡ y

apd : ∀{i j}{A : Set i}{B : A → Set j}(f : (x : A) → B x){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
    → f a₀ =[ ap B a₂ ]= f a₁
apd f refl = refl

apd2 : ∀{i j k}{A : Set i}{B : A → Set j}{C : Set k}(f : (x : A) → B x → C)
  {x₁ y₁ : A}{x₂ : B x₁}{y₂ : B y₁}(e : x₁ ≡ y₁) → transport B e x₂ ≡ y₂ → f x₁ x₂ ≡ f y₁ y₂
apd2 f refl refl = refl

apd3 : ∀{i j k l}{A : Set i}{B : A → Set j}{C : (x : A) → B x → Set k}{D : Set l}(f : (x : A)(y : B x) → C x y → D)
  {x₀ x₁ : A}(x₀₁ : x₀ ≡ x₁)
  {y₀ : B x₀}{y₁ : B x₁}(y₀₁ : transport B x₀₁ y₀ ≡ y₁)
  {z₀ : C x₀ y₀}{z₁ : C x₁ y₁}(z₀₁ : coe (apd2 C x₀₁ y₀₁) z₀ ≡ z₁) →
  f x₀ y₀ z₀ ≡ f x₁ y₁ z₁
apd3 f refl refl refl = refl

data Maybe {i}(A : Set i) : Set i where
  Nothing  : Maybe A
  Just     : A → Maybe A

postulate
  exercise  : ∀{i}{A : Set  i} → A
  exercisep : ∀{i}{A : Prop i} → A
\end{code}
