\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module SystemT.LogicalPredicate where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)
open import SystemT.Algebra
open import SystemT.Standard

open I
open Algebra St using (⟦_⟧C ; ⟦_⟧T ; ⟦_⟧S ; ⟦_⟧t)
\end{code}
\begin{code}
data PNat : Tm ∙ Nat → Set where
  Pzero : PNat zero
  Psuc : ∀ {n'} → PNat n' → PNat (suc n')

indPNat : ∀ {i} (P : ∀ {n'} → PNat n' → Set i) →
  P Pzero → (∀ {n'} {n : PNat n'} → P n → P (Psuc n)) →
  ∀ {n'} (n : PNat n') → P n
indPNat P u v Pzero = u
indPNat P u v (Psuc n) = v (indPNat P u v n)

recPNat : ∀ {i} (A : Tm ∙ Nat → Set i) →
  A zero → (∀ {n'} → A n' → A (suc n')) →
  ∀ {n'} → PNat n' → A n'
recPNat A u v n = indPNat (λ {n'} _ → A n') u v n

Nat-Prec : ∀ {i A'} (A : Tm ∙ A' → Set i)
  {u' : Tm ∙ A'}{v' : Tm (∙ ▹ A') A'} →
  A u' → (∀ {t'} → A t' → A (v' [ id , t' ])) →
  ∀ {n'} → PNat n' → A (rec u' v' n')
Nat-Prec A {u'}{v'} u v n = recPNat (λ n' → A (rec u' v' n')) u v n

LP : DepAlgebra
LP = record
         { Con = λ Γ' → Sub ∙ Γ' → Set
         ; Ty = λ A' → Tm ∙ A' → Set
         ; Sub = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
         ; Tm = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
         ; ∙ = λ _ → ↑p 𝟙
         ; _▹_ = λ Γ A σ' → Γ (p ∘ σ') × A (q [ σ' ])
         ; Nat = PNat
         ; _⇒_ = λ A B t' → ∀ {u'} → A u' → B (t' $ u')
         ; _∘_ = λ σ δ x → σ (δ x)
         ; id = idf
         ; ε = λ _ → *↑
         ; _,_ = λ σ t x → σ x ,Σ t x
         ; p = π₁
         ; q = π₂
         ; _[_] = λ t σ x → t (σ x)
         ; lam = λ t x y → t (x ,Σ y)
         ; app = λ t xy → t (π₁ xy) (π₂ xy)
         ; zero = λ _ → Pzero
         ; suc = λ n x → Psuc (n x)
         ; rec = λ { {A = A} u v n x →
                   Nat-Prec A (u x) (λ y → v (x ,Σ y)) (n x) }
         ; ass = refl
         ; idl = refl
         ; idr = refl
         ; ∙η = refl
         ; ▹β₁ = refl
         ; ▹β₂ = refl
         ; ▹η = refl
         ; [id] = refl
         ; [∘] = refl
         ; ⇒β = refl
         ; ⇒η = refl
         ; Natβ₁ = refl
         ; Natβ₂ = refl
         ; lam[] = refl
         ; zero[] = refl
         ; suc[] = refl
         ; rec[] = refl
         }
module LP = DepAlgebra LP

pNat : ∀ n → PNat n
pNat n = LP.⟦ n ⟧t {id} *↑

completeness : ∀ {n} → ⌜ eval n ⌝ ≡ n
completeness {n} = 
  let A n = ↑p (⌜ eval n ⌝ ≡ n)
  in  ↓[ recPNat A refl↑ (λ n → ↑[ ap suc ↓[ n ]↓ ]↑) (pNat n) ]↓
\end{code}
