\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Ind2.Standard where

open import Lib renaming (_∘_ to _∘f_ ; _×_ to _×m_ ; _,_ to _⸴_)
open import Ind2.Algebra
\end{code}
\begin{code}
module M where
  data Ar (C : Set) : Set where
    X   : Ar C
    X⇒_ : Ar C → Ar C
    C⇒_ : Ar C → Ar C

  data Sign (C : Set) : Set where
    ◆   : Sign C
    _▷_ : Sign C → Ar C → Sign C

  infixr 6 X⇒_
  infixr 6 C⇒_
  infixl 5 _▷_

  data VarI (C : Set) : Sign C → Ar C → Set where
    vvz : ∀ {Γ A} → VarI C (Γ ▷ A) A
    vvs : ∀ {Γ A B} → VarI C Γ A → VarI C (Γ ▷ B) A

  data TmI (C : Set) : Sign C → Ar C → Set where
    var : ∀ {Γ A} → VarI C Γ A → TmI C Γ A
    _$_ : ∀ {Γ A} → TmI C Γ (X⇒ A) → TmI C Γ X → TmI C Γ A
    _$̂_ : ∀ {Γ A} → TmI C Γ (C⇒ A) → C → TmI C Γ A

  data SubI (C : Set) : Sign C → Sign C → Set where
    ε   : ∀ {Γ} → SubI C Γ ◆
    _,_ : ∀ {Γ Δ A} → SubI C Γ Δ → TmI C Γ A → SubI C Γ (Δ ▷ A)

  infixl 6 _$_
  infixl 6 _$̂_
  infixl 5 _,_

  vz : ∀ {C Γ A} → TmI C (Γ ▷ A) A
  vz = var vvz

  vs : ∀ {C Γ A B} → TmI C Γ A → TmI C (Γ ▷ B) A
  vs (var x) = var (vvs x)
  vs (t $ u) = vs t $ vs u
  vs (t $̂ c) = vs t $̂ c

  wk : ∀ {C Γ Δ A} → SubI C Γ Δ → SubI C (Γ ▷ A) Δ
  wk ε       = ε
  wk (σ , t) = wk σ , vs t

  id : ∀ {C Γ} → SubI C Γ Γ
  id {Γ = ◆}     = ε
  id {Γ = Γ ▷ A} = wk id , vz

  _ᴬA : ∀ {C} → Ar C → Set → Set
  (X      ᴬA) T = T
  ((X⇒ A) ᴬA) T = T → (A ᴬA) T
  _ᴬA {C} (C⇒ A) T = C → (A ᴬA) T

  _ᴬ : ∀ {C} → Sign C → Set → Set
  (◆       ᴬ) A = ↑p 𝟙
  ((Γ ▷ A) ᴬ) T = (Γ ᴬ) T ×m (A ᴬA) T

  _ᴬv : ∀ {C Γ A} → VarI C Γ A → ∀ {T} → (Γ ᴬ) T → (A ᴬA) T
  (vvz   ᴬv) (γ ⸴ α) = α
  (vvs x ᴬv) (γ ⸴ β) = (x ᴬv) γ
  
  _ᴬt : ∀ {C Γ A} → TmI C Γ A → ∀ {T} → (Γ ᴬ) T → (A ᴬA) T
  (var x   ᴬt) γ = (x ᴬv) γ
  ((t $ u) ᴬt) γ = (t ᴬt) γ ((u ᴬt) γ)
  ((t $̂ c) ᴬt) γ = (t ᴬt) γ c

  _ᴬs : ∀ {C Γ Δ} → SubI C Γ Δ → ∀ {T} → (Γ ᴬ) T → (Δ ᴬ) T
  (ε       ᴬs) γ = *↑
  ((σ , t) ᴬs) γ = (σ ᴬs) γ ⸴ (t ᴬt) γ

  vsᴬ : ∀ {C Γ A B T} {t : TmI C Γ A} → (vs {B = B} t ᴬt){T} ≡ (t ᴬt) ∘f π₁
  vsᴬ {t = var x}  = refl
  vsᴬ {t = t $ u}  = ap2 _∘'_ (vsᴬ {t = t}) (vsᴬ {t = u})
  vsᴬ {t = t $̂ c}  = ap (λ f x → f x c) (vsᴬ {t = t})

  wkᴬ : ∀ {C Γ Δ A T} {σ : SubI C Γ Δ} → (wk {A = A} σ ᴬs){T} ≡ (σ ᴬs) ∘f π₁
  wkᴬ {σ = ε}     = refl
  wkᴬ {σ = σ , t} = ap2 (λ f g x → f x ⸴ g x) (wkᴬ {σ = σ}) (vsᴬ {t = t})

  idᴬ : ∀ {C T} {Γ : Sign C} → (id {Γ = Γ} ᴬs){T} ≡ idf
  idᴬ {Γ = ◆}     = refl
  idᴬ {Γ = Γ ▷ A} = ap (λ f γ → f γ ⸴ π₂ γ)
                       (wkᴬ {σ = id} ◾ ap (_∘f π₁) (idᴬ {Γ = Γ}))

  ind : ∀ {C} → Sign C → Set
  ind {C} Ω = TmI C Ω X

  conᵗ : ∀ {C Ω A} → TmI C Ω A → (A ᴬA) (ind Ω)
  conᵗ {A = X}    t = t
  conᵗ {A = X⇒ A} f = λ u → conᵗ (f $ u)
  conᵗ {A = C⇒ A} f = λ c → conᵗ (f $̂ c)

  conˢ : ∀ {C Ω Γ} → SubI C Ω Γ → (Γ ᴬ) (ind Ω)
  conˢ ε       = *↑
  conˢ (σ , t) = conˢ σ ⸴ conᵗ t

  con : ∀ {C} (Ω : Sign C) → (Ω ᴬ) (ind Ω)
  con Ω = conˢ id

  rec : ∀ {C T} (Ω : Sign C) → (Ω ᴬ) T → ind Ω → T
  rec Ω ω t = (t ᴬt) ω

  
{-
  _ⱽ : ∀ {C} → Sign C → Sign C → Set
  (◆ ⱽ) Ω' = ↑p 𝟙
  _ⱽ {C} (Ω ▷ R) Ω' = (Ω ⱽ) Ω' ×m VarI C Ω' R

  shift : ∀ {C} {Ω Ω' : Sign C}{Q : Ar C} → (Ω ⱽ) Ω' → (Ω ⱽ) (Ω' ▷ Q)
  shift {Ω = ◆} .*↑ = *↑
  shift {Ω = Ω ▷ R} (α ,Σ v) = shift α ,Σ vs v

  conVar : ∀ {C} (Ω : Sign C) → (Ω ⱽ) Ω
  conVar ◆ = *↑
  conVar (Ω ▷ R) = shift (conVar Ω) ,Σ vz

  conA : ∀ {C} {R : Ar C}{Ω' : Sign C} → TmI C Ω' R → (R ᴬA) (ind Ω')
  conA {R = X} t = t
  conA {R = X⇒ R} f = λ u → conA (f $ u)
  conA {R = C⇒ R} f = λ a → conA (f $̂ a)

  con' : ∀ {C} {Ω Ω' : Sign C}→ (Ω ⱽ) Ω' → (Ω ᴬ) (ind Ω')
  con' {Ω = ◆} .*↑ = *↑
  con' {Ω = Ω ▷ R} (α ,Σ v) = con' α ,Σ conA (var v)

  con : ∀ {C} (Ω : Sign C) → (Ω ᴬ) (ind Ω)
  con Ω = con' (conVar Ω)

  recVar : ∀ {A C} {R : Ar C}{Ω : Sign C} → (Ω ᴬ) A → VarI C Ω R → (R ᴬA) A
  recVar (ω ,Σ c) vz = c
  recVar (ω ,Σ c) (vs v) = recVar ω v

  rec' : ∀ {A C} {R : Ar C}{Ω : Sign C} → (Ω ᴬ) A → TmI C Ω R → (R ᴬA) A
  rec' ω (var v) = recVar ω v
  rec' ω (f $ u) = rec' ω f (rec' ω u)
  rec' ω (f $̂ a) = rec' ω f a

  rec : ∀ {A C} (Ω : Sign C) → (Ω ᴬ) A → ind Ω → A
    rec Ω ω t = rec' ω t
-}
  _ᴹA : ∀ {Γ : Set} {A B C} (R : Ar C) →
    ((Γ → A) → (Γ → B)) → (Γ → (R ᴬA) A) → (Γ → (R ᴬA) B) → Prop
  (X      ᴹA) F t u = F t ≡ u
  ((X⇒ R) ᴹA) F f g = ∀ t → (R ᴹA) F (f ∘' t) (g ∘' F t)
  ((C⇒ R) ᴹA) F f g = ∀ t → (R ᴹA) F (f ∘' t) (g ∘' t)

  _ᴹ : ∀ {Γ : Set} {A B C} (Ω : Sign C) →
    ((Γ → A) → (Γ → B)) → (Γ → (Ω ᴬ) A) → (Γ → (Ω ᴬ) B) → Prop
  (◆       ᴹ) F t u = 𝟙'
  ((Ω ▷ R) ᴹ) F t u =
    (Ω ᴹ) F (π₁ ∘f t) (π₁ ∘f u) ×p (R ᴹA) F (π₂ ∘f t) (π₂ ∘f u)
{-
  postulate
    indβ : ∀ {Γ : Set} {A C} {Ω : Sign C} {ω : Γ → (Ω ᴬ) A} →
      (Ω ᴹ) (λ _ → con Ω) ω (λ t γ → rec Ω (ω γ) (t γ))
-}

  module _ {Γ : Set} {A C} {Ω : Sign C} {ω : Γ → (Ω ᴬ) A} where
    F : (Γ → ind Ω) → (Γ → A)
    F u γ = rec Ω (ω γ) (u γ)

    Ι : Γ → (Ω ᴬ) (ind Ω)
    Ι _ = con Ω

    indβᵗ : ∀ {R} (t : TmI C Ω R) → (R ᴹA) F ((t ᴬt) ∘f Ι) ((t ᴬt) ∘f ω)
    indβᵗ {X}    t = {!!}
    indβᵗ {X⇒ R} t = {!!}
    indβᵗ {C⇒ R} t = λ c → indβᵗ (t $̂ c {!!})

    indβˢ : ∀ {Ψ} (σ : SubI C Ω Ψ) → (Ψ ᴹ) F ((σ ᴬs) ∘f Ι) ((σ ᴬs) ∘f ω)
    indβˢ ε       = *'
    indβˢ (σ , t) = indβˢ σ ,p indβᵗ t
    
    indβ : (Ω ᴹ) F Ι ω
    indβ = transportp2 ((Ω ᴹ) F)
                       (ap (_∘f Ι) idᴬ)
                       (ap (_∘f ω) idᴬ)
                       (indβˢ id)

{-
  N : Sign (↑p 𝟘)
  N = ◆ ▷ X ▷ X⇒ X

  Nat : Set
  Nat = ind N

  zero : Nat
  zero = π₂ (π₁ (con N))

  suc : Nat → Nat
  suc = π₂ (con N)

  Nβ : {Γ A : Set} {ω : Γ → (N ᴬ) A} →
    (N ᴹ) (λ _ → con N) ω (λ t γ → rec N (ω γ) (t γ))
  Nβ = *' ,p refl ,p refl
-}

St : Algebra
St = record
       { Ty = Set
       ; Con = Set
       ; Ar = M.Ar
       ; Sign = M.Sign
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; Unit = ↑p 𝟙
       ; Empty = ↑p 𝟘
       ; _⇒_ = λ A B → A → B
       ; _×_ = _×m_
       ; _+_ = _⊎_
       ; ind = M.ind
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×m_
       ; X = M.X
       ; X⇒_ = M.X⇒_
       ; C⇒_ = M.C⇒_
       ; ◆ = M.◆
       ; _▷_ = M._▷_
       ; _ᴬA = M._ᴬA
       ; _ᴬ = M._ᴬ
       ; _ᴹA = M._ᴹA
       ; _ᴹ = M._ᴹ
       ; _∘_ = _∘f_ 
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ⸴ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; lam = λ t γ a → t (γ ⸴ a)
       ; app = λ t γa → t (π₁ γa) (π₂ γa)
       ; tt = const *↑
       ; ⟨_,_⟩ = λ u v γ → u γ ⸴ v γ
       ; proj₁ = λ t γ → π₁ (t γ)
       ; proj₂ = λ t γ → π₂ (t γ)
       ; absurd = λ t γ → ⟦ ↓[ t γ ]↓ ⟧𝟘
       ; inl = λ t γ → ι₁ (t γ)
       ; inr = λ t γ → ι₂ (t γ)
       ; case = λ t u v γ →
                  case⊎ (t γ) (λ a → u (γ ⸴ a)) (λ b → v (γ ⸴ b))
       ; con = λ Ω γ → M.con Ω
       ; rec = λ Ω ω u γ → M.rec Ω (ω γ) (u γ)
       ; ᴬAX = refl
       ; ᴬAX⇒ = refl
       ; ᴬAC⇒ = refl
       ; ᴬ◆ = refl
       ; ᴬ▷ = refl
       ; ᴹAX = refl
       ; ᴹAX⇒ = refl
       ; ᴹAC⇒ = refl
       ; ᴹ◆ = refl
       ; ᴹ▷ = refl
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; uη = refl
       ; ×β₁ = refl
       ; ×β₂ = refl
       ; ×η = refl
       ; +β₁ = refl
       ; +β₂ = refl
       ; indβ = {!!}
       ; lam[] = refl
       ; tt[] = refl
       ; ⟨,⟩[] = refl
       ; absurd[] = refl
       ; inl[] = refl
       ; inr[] = refl
       ; case[] = refl
       ; con[] = refl
       ; rec[] = refl
       }
module St = Algebra St
\end{code}
