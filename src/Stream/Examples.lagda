\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Stream.Examples where

open import Lib hiding (_∘_ ; _,_ ; *)
open import Stream.Algebra
open import Stream.Standard

open I

q1 : ∀ {Γ A B} → Tm (Γ ▹ A ▹ B) A
q1 = q [ p ]
\end{code}
\begin{code}
times2plus1 : ∀ {Γ} → Tm Γ (Nat ⇒ Nat)
times2plus1 = lam (rec (suc zero) (suc (suc q)) q)

_*2+1 : ℕ → ℕ
_*2+1 = eval times2plus1

plus : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
plus = lam (lam (rec q (suc q) q1))

_+_ : ℕ → ℕ → ℕ
_+_ = eval plus

times : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
times = lam (lam (rec zero (app (app plus) [ id , q1 , q ]) q1))

_*_ : ℕ → ℕ → ℕ
_*_ = eval times

pred : ∀ {Γ} → Tm Γ (Nat ⇒ Nat)
pred = lam (rec (lam zero) (lam (q $ (q1 $ lam (suc q)))) q $ lam q)

P : ℕ → ℕ
P = eval pred
\end{code}
