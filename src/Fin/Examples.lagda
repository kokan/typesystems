\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Fin.Examples where

open import Lib
open import Fin.Algebra
open import Fin.Standard
open import Fin.LogicalPredicate

open I
\end{code}
\begin{code}
Bool : Ty
Bool = Unit + Unit

true : ∀ {Γ} → Tm Γ Bool
true = inl tt

false : ∀ {Γ} → Tm Γ Bool
false = inr tt

ite : ∀ {Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
ite b u v = case b (u [ p ]) (v [ p ])

canonicityB : {b : Tm ∙ Bool} → ↑p (b ≡ true) ⊎ ↑p (b ≡ false)
canonicityB {b} =
  let P t = ↑p (b ≡ inl t) ⊎ ↑p (b ≡ inr t)
  in  case⊎ (canonicity+ {t = b})
            (λ a → transport P uη (ι₁ (π₂ a)))
            (λ b → transport P uη (ι₂ (π₂ b)))

evalB : Tm ∙ Bool → 𝟚
evalB b = case⊎ (eval b) (λ _ → I) (λ _ → O)

⌜_⌝B : 𝟚 → Tm ∙ Bool
⌜ O ⌝B = false
⌜ I ⌝B = true

completenessB : ∀ {b} → ⌜ evalB b ⌝B ≡ b
completenessB {b} =
  let P b = ↑p (⌜ evalB b ⌝B ≡ b)
  in  ↓[ case⊎ (canonicityB {b})
               (λ e → transport P (↓[ e ]↓ ⁻¹) refl↑)
               (λ e → transport P (↓[ e ]↓ ⁻¹) refl↑) ]↓
\end{code}
