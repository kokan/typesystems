\chapter{Inductive types}

\begin{tcolorbox}[title=Learning goals of this chapter]
  Inductive types by examples: natural numbers, lists, binary trees.
  Type formers, constructors, recursor, defining functions by recursion. 
\end{tcolorbox}

TODO: definablility.

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_; _×_ to _⊗_)

module Ind where

module I where
  data Ty      : Set where
    Bool       : Ty
    Nat        : Ty
    _⇒_        : Ty → Ty → Ty
    Unit       : Ty
    _×_        : Ty → Ty → Ty
    List       : Ty → Ty
    Tree       : Ty → Ty

  data Con     : Set where
    ∙          : Con
    _▹_        : Con → Ty → Con

  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×_

  postulate
    Sub        : Con → Con → Set
    Tm         : Con → Ty → Set

    _∘_        : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id         : ∀{Γ} → Sub Γ Γ
    ass        : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                 (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl        : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr        : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]       : ∀{Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id]       : ∀{Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘]        : ∀{Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                 t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ε          : ∀{Γ} → Sub Γ ∙
    ∙η         : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε

    _,_        : ∀{Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p          : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q          : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η         : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ

    true       : ∀{Γ} → Tm Γ Bool
    false      : ∀{Γ} → Tm Γ Bool
    ite        : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    Boolβ₁     : ∀ {Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    Boolβ₂     : ∀ {Γ A} {u v : Tm Γ A} → ite false u v ≡ v
    true[]     : ∀ {Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    false[]    : ∀ {Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    ite[]      : ∀ {Γ Δ A} {b : Tm Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
                 (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])

    zero       : ∀{Γ} → Tm Γ Nat
    suc        : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    recNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → recNat u v zero ≡ u
    Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} → recNat u v (suc t) ≡ v [ id , recNat u v t ]
    zero[]     : ∀{Γ Δ}{σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[]      : ∀{Γ Δ}{n : Tm Δ Nat}{σ : Sub Γ Δ} → (suc n) [ σ ] ≡ suc (n [ σ ])
    recNat[]   : ∀{Γ Δ A}{u : Tm Δ A}{v : Tm (Δ ▹ A) A}{t : Tm Δ Nat}{σ : Sub Γ Δ} →
                 recNat u v t [ σ ] ≡ recNat (u [ σ ]) (v [ σ ∘ p , q ]) (t [ σ ])

    lam        : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app        : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    ⇒β         : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η         : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[]      : ∀{Γ Δ A B}{t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
                 (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    tt         : ∀{Γ} → Tm Γ Unit
    Unitη      : ∀{Γ}{t : Tm Γ Unit} → t ≡ tt

    ⟨_,_⟩      : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁      : ∀{Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂      : ∀{Γ A B} → Tm Γ (A × B) → Tm Γ B
    ×β₁        : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂        : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η         : ∀{Γ A B}{t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
    ⟨,⟩[]      : ∀{Γ Δ A B}{u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
                 ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    nil        : ∀{Γ A} → Tm Γ (List A)
    cons       : ∀{Γ A} → Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
    recList    : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (List A) → Tm Γ B
    Listβ₁     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → recList u v nil ≡ u
    Listβ₂     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
                 recList u v (cons t₁ t) ≡ (v [ id , t₁ , recList u v t ])
    nil[]      : ∀{Γ Δ A}{σ : Sub Γ Δ} → nil {Δ}{A} [ σ ] ≡ nil {Γ}{A}
    cons[]     : ∀{Γ Δ A}{t₁ : Tm Δ A}{t : Tm Δ (List A)}{σ : Sub Γ Δ} →
                 (cons t₁ t) [ σ ] ≡ cons (t₁ [ σ ]) (t [ σ ])
    recList[]  : ∀{Γ Δ A B}{u : Tm Δ B}{v : Tm (Δ ▹ A ▹ B) B}{t : Tm Δ (List A)}{σ : Sub Γ Δ} →
                 recList u v t [ σ ] ≡ recList (u [ σ ]) (v [ (σ ∘ p , q) ∘ p , q ]) (t [ σ ])

    leaf       : ∀{Γ A} → Tm Γ A → Tm Γ (Tree A)
    node       : ∀{Γ A} → Tm Γ (Tree A) → Tm Γ (Tree A) → Tm Γ (Tree A)
    recTree    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm (Γ ▹ B ▹ B) B → Tm Γ (Tree A) → Tm Γ B
    Treeβ₁     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{a : Tm Γ A} → recTree l n (leaf a) ≡ l [ id , a ]
    Treeβ₂     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{ll rr : Tm Γ (Tree A)} →
                 recTree l n (node ll rr) ≡ n [ id , recTree l n ll , recTree l n rr ]
    leaf[]     : ∀{Γ Δ A}{a : Tm Δ A}{σ : Sub Γ Δ} → (leaf a) [ σ ] ≡ leaf (a [ σ ])
    node[]     : ∀{Γ Δ A}{ll rr : Tm Δ (Tree A)}{σ : Sub Γ Δ} →
                 (node ll rr) [ σ ] ≡ node (ll [ σ ]) (rr [ σ ])
    recTree[]  : ∀{Γ Δ A B}{l : Tm (Δ ▹ A) B}{n : Tm (Δ ▹ B ▹ B) B}{t : Tm Δ (Tree A)}{σ : Sub Γ Δ} →
                 recTree l n t [ σ ] ≡ recTree (l [ (σ ∘ p) , q ]) (n [ (σ ∘ p ∘ p) , (q [ p ]) , q ]) (t [ σ ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = app t [ id , u ]

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  tt[] : ∀{Γ Δ}{σ : Sub Γ Δ} → tt [ σ ] ≡ tt
  tt[] = Unitη

  proj₁[] : ∀{Γ Δ A B}{t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    proj₁ t [ σ ] ≡ proj₁ (t [ σ ])
  proj₁[] {Γ}{Δ}{A}{B}{t}{σ} = ×β₁ ⁻¹ ◾ ap proj₁ ⟨,⟩[] ⁻¹ ◾ ap (λ u → proj₁ (u [ σ ])) ×η

  proj₂[] : ∀{Γ Δ A B}{t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    proj₂ t [ σ ] ≡ proj₂ (t [ σ ])
  proj₂[] {Γ}{Δ}{A}{B}{t}{σ} = ×β₂ ⁻¹ ◾ ap proj₂ ⟨,⟩[] ⁻¹ ◾ ap (λ u → proj₂ (u [ σ ])) ×η

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE Boolβ₁ Boolβ₂  true[] false[] ite[] #-}
  {-# REWRITE Natβ₁ Natβ₂  zero[] suc[] recNat[] #-}
  {-# REWRITE ⇒β ⇒η lam[] app[] #-}
  {-# REWRITE tt[] #-}
  {-# REWRITE ×β₁ ×β₂ ×η ⟨,⟩[] proj₁[] proj₂[] #-}
  {-# REWRITE Listβ₁ Listβ₂ nil[] cons[] recList[] #-}
  {-# REWRITE Treeβ₁ Treeβ₂ leaf[] node[] recTree[] #-}

record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×_

  field
    Con        : Set i
    Sub        : Con → Con → Set k
    Ty         : Set j
    Tm         : Con → Ty → Set l

    _∘_        : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id         : ∀{Γ} → Sub Γ Γ
    ass        : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                 (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl        : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr        : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]       : ∀{Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id]       : ∀{Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘]        : ∀{Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                 t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ∙          : Con
    ε          : ∀{Γ} → Sub Γ ∙
    ∙η         : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε

    _▹_        : Con → Ty → Con
    _,_        : ∀{Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p          : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q          : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η         : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ

    Bool       : Ty
    true       : ∀{Γ} → Tm Γ Bool
    false      : ∀{Γ} → Tm Γ Bool
    ite        : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    Boolβ₁     : ∀ {Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    Boolβ₂     : ∀ {Γ A} {u v : Tm Γ A} → ite false u v ≡ v
    true[]     : ∀ {Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    false[]    : ∀ {Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    ite[]      : ∀ {Γ Δ A} {b : Tm Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
                 (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
\end{code}

An inductive type is specified by its type introduction operator and
its constructors. Its eliminator and computation rules are determined
by the constructors: the eliminator says that for any type \verb$C$
and elements of that type which have the shape of the constructors,
there is a function (called recursor) from the inductive type to
\verb$C$. The computation rules say that if we apply the recursor to a
constructor, we obtain the corresponding element.

We've already seen some inductive types: \verb$Bool$ had two
constructors \verb$true$ and \verb$false$, and its recursor was called
if-then-else, it said that for any type \verb$C$ and two elements of
\verb$C$, there is a function from \verb$Bool$ to \verb$C$. For any
two types \verb$A$ and \verb$B$, \verb$A + B$ was an inductive type
with constructors \verb$inl$ and \verb$inr$. Its recursor was called
\verb$case$, it said that for any type \verb$C$ and elements \verb$A → C$
and \verb$B → C$, there is a function from \verb$A + B$ to \verb$C$.

Natural numbers are also an inductive type: the constructors are
\verb$zero$ and \verb$suc$, the latter can be written in several
different equivalent ways:
\begin{verbatim}
suc : Tm Γ Nat → Tm Γ Nat
suc : Tm Γ (Nat ⇒ Nat)
suc : Tm (Γ ▹ Nat) Nat
\end{verbatim}
When specifying the arguments of the recursor, we use the last version
which does not refer to metatheoretic or object theoretic function space.
So the eliminator says that given a type \verb$C$, a term in \verb$Tm Γ C$
and a term in \verb$Tm (Γ ▹ C) C$, we get a function from \verb$Tm Γ Nat$ to
\verb$Tm Γ C$.

A language has natural numbers if it extends the substitution calculus
with the following operators and equations:
\begin{code}
    Nat        : Ty
    zero       : ∀{Γ} → Tm Γ Nat
    suc        : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    recNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → recNat u v zero ≡ u
    Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                 recNat u v (suc t) ≡ v [ id , recNat u v t ]
    zero[]     : ∀{Γ Δ}{σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[]      : ∀{Γ Δ}{n : Tm Δ Nat}{σ : Sub Γ Δ} → (suc n) [ σ ] ≡ suc (n [ σ ])
    recNat[]   : ∀{Γ Δ A}{u : Tm Δ A}{v : Tm (Δ ▹ A) A}{t : Tm Δ Nat}{σ : Sub Γ Δ} →
                 recNat u v t [ σ ] ≡ recNat (u [ σ ]) (v [ σ ∘ p , q ]) (t [ σ ])
\end{code}
The computation rules \verb$Natβ₁$ and \verb$Natβ₂$ express that
\verb$recNat u v t$ works as follows: it replaces all \verb$suc$s in
\verb$t$ by what is specified by \verb$v$, and replaces \verb$zero$ by
\verb$u$. For example, \verb$recNat u v (suc (suc (suc zero)))$ is
equal to \verb$v [ id , v [ id , v [ id , u ] ] ]$ (which can be thought about as
\verb$v (v (v u))$ but we use substitution to express the applications).

Our previous definition of \verb$Nat$ wasn't an inductive type because
it didn't have a recursor, only two special cases of the recursor: addition
and \verb$isZero$. Both of these can be defined using the recursor, see below.
\begin{code}[hide]
    _⇒_        : Ty → Ty → Ty
    lam        : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app        : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    ⇒β         : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η         : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[]      : ∀{Γ Δ A B}{t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
                 (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    Unit       : Ty
    tt         : ∀{Γ} → Tm Γ Unit
    Unitη      : ∀{Γ}{t : Tm Γ Unit} → t ≡ tt

    _×_        : Ty → Ty → Ty
    ⟨_,_⟩      : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁      : ∀{Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂      : ∀{Γ A B} → Tm Γ (A × B) → Tm Γ B
    ×β₁        : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂        : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η         : ∀{Γ A B}{t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
    ⟨,⟩[]      : ∀{Γ Δ A B}{u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
                 ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

\end{code}

Another inductive type is that of lists. For any type \verb$A$, we have a
type \verb$List A$. It has two constructors \verb$List : Tm Γ (List A)$ and
\verb$cons$ which again can be expressed in three different ways. We will use
the first one when specifying the constructor and the third one when specifying
the arguments of the recursor.
\begin{verbatim}
cons : Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
cons : Tm Γ (A ⇒ List A ⇒ List A)
cons : Tm (Γ ▹ A ▹ List A) (List A)
\end{verbatim}

A language has lists if it extends the substitution calculus with the following
operators and equations:
\begin{code}
    List       : Ty → Ty
    nil        : ∀{Γ A} → Tm Γ (List A)
    cons       : ∀{Γ A} → Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
    recList    : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (List A) → Tm Γ B
    Listβ₁     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → recList u v nil ≡ u
    Listβ₂     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
                 recList u v (cons t₁ t) ≡ (v [ id , t₁ , recList u v t ])
    nil[]      : ∀{Γ Δ A}{σ : Sub Γ Δ} → nil {Δ}{A} [ σ ] ≡ nil {Γ}{A}
    cons[]     : ∀{Γ Δ A}{t₁ : Tm Δ A}{t : Tm Δ (List A)}{σ : Sub Γ Δ} →
                 (cons t₁ t) [ σ ] ≡ cons (t₁ [ σ ]) (t [ σ ])
    recList[]  : ∀{Γ Δ A B}{u : Tm Δ B}{v : Tm (Δ ▹ A ▹ B) B}{t : Tm Δ (List A)}{σ : Sub Γ Δ} →
                 recList u v t [ σ ] ≡ recList (u [ σ ]) (v [ (σ ∘ p , q) ∘ p , q ]) (t [ σ ])
\end{code}
In \verb$recList u v t$, \verb$u$ expresses what to do when \verb$t = nil t'$,
\verb$v$ expresses what to do when \verb$t = cons t' t''$.

Another inductive type is that of binary trees with information at the
leaves. For any type \verb$A$, \verb$Ty A$ is a type, the constructors are
\verb$leaf$ and \verb$node$. A language has these binary trees if it has the
following operators and equations:
\begin{code}
    Tree       : Ty → Ty
    leaf       : ∀{Γ A} → Tm Γ A → Tm Γ (Tree A)
    node       : ∀{Γ A} → Tm Γ (Tree A) → Tm Γ (Tree A) → Tm Γ (Tree A)
    recTree    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm (Γ ▹ B ▹ B) B → Tm Γ (Tree A) → Tm Γ B
    Treeβ₁     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{a : Tm Γ A} →
                 recTree l n (leaf a) ≡ l [ id , a ]
    Treeβ₂     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{ll rr : Tm Γ (Tree A)} →
                 recTree l n (node ll rr) ≡ n [ id , recTree l n ll , recTree l n rr ]
    leaf[]     : ∀{Γ Δ A}{a : Tm Δ A}{σ : Sub Γ Δ} → (leaf a) [ σ ] ≡ leaf (a [ σ ])
    node[]     : ∀{Γ Δ A}{ll rr : Tm Δ (Tree A)}{σ : Sub Γ Δ} →
                 (node ll rr) [ σ ] ≡ node (ll [ σ ]) (rr [ σ ])
    recTree[]  : ∀{Γ Δ A B}{l : Tm (Δ ▹ A) B}{n : Tm (Δ ▹ B ▹ B) B}{t : Tm Δ (Tree A)}{σ : Sub Γ Δ} →
                 recTree l n t [ σ ] ≡ recTree (l [ (σ ∘ p) , q ]) (n [ (σ ∘ p ∘ p) , (q [ p ]) , q ]) (t [ σ ])
\end{code}
\begin{code}[hide]
  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = app t [ id , u ]

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  def⇒ : ∀ {Γ A B} {t : Tm Γ A}{u : Tm (Γ ▹ A) B} → def t u ≡ lam u $ t
  def⇒ = ap (def _) ⇒β ⁻¹

  ⇒η' : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  ⇒η' {t = t} = ap lam
                ( ap (_[ id , q ]) app[]
                ◾ [∘]
                ◾ ap ((app t) [_])
                  ( ,∘
                  ◾ ap2 _,_
                    ( ass
                    ◾ ap (p ∘_) ▹β₁)
                    ( ▹β₂
                    ◾ [id] ⁻¹)
                  ◾ ▹η)
                ◾ [id])
              ◾ ⇒η

  def[] : ∀ {Γ Δ A B} {t : Tm Δ A}{u : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▹β₁
            ◾ idr)
          ▹β₂))
    refl

  $[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
    (t $ u) [ σ ] ≡ t [ σ ] $ u [ σ ]
  $[] = def[] ◾ ap (def _) app[] ⁻¹

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧T : I.Ty → Ty
  ⟦ I.Bool ⟧T = Bool
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.List A ⟧T = List ⟦ A ⟧T
  ⟦ I.Tree A ⟧T = Tree ⟦ A ⟧T
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ I.Unit ⟧T = Unit
  ⟦ A I.× B ⟧T = ⟦ A ⟧T × ⟦ B ⟧T

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T

    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
    ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
    ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ I.Bool}{u v : I.Tm Γ A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦recNat⟧ : ∀{Γ A}{u : I.Tm Γ A}{v : I.Tm (Γ I.▹ A) A}{t : I.Tm Γ I.Nat} →
      ⟦ I.recNat u v t ⟧t ≡ recNat ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦recNat⟧ #-}

    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦tt⟧ : ∀ {Γ} → ⟦ I.tt {Γ} ⟧t ≡ tt
    {-# REWRITE ⟦tt⟧ #-}

    ⟦⟨,⟩⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.⟨ u , v ⟩ ⟧t ≡ ⟨ ⟦ u ⟧t , ⟦ v ⟧t ⟩
    ⟦proj₁⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₁ t ⟧t ≡ proj₁ ⟦ t ⟧t
    ⟦proj₂⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₂ t ⟧t ≡ proj₂ ⟦ t ⟧t
    {-# REWRITE ⟦⟨,⟩⟧ ⟦proj₁⟧ ⟦proj₂⟧ #-}

    ⟦nil⟧ : ∀ {Γ A} → ⟦ I.nil {Γ}{A} ⟧t ≡ nil
    ⟦cons⟧ : ∀ {Γ A} {t₁ : I.Tm Γ A}{t : I.Tm Γ (I.List A)} →
      ⟦ I.cons t₁ t ⟧t ≡ cons ⟦ t₁ ⟧t ⟦ t ⟧t
    ⟦recList⟧ : ∀{Γ A B}{u : I.Tm Γ B}{v : I.Tm (Γ I.▹ A I.▹ B) B}{t : I.Tm Γ (I.List A)} →
      ⟦ I.recList u v t ⟧t ≡ recList ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦nil⟧ ⟦cons⟧ ⟦recList⟧ #-}

    ⟦leaf⟧ : ∀ {Γ A}{t : I.Tm Γ A} → ⟦ I.leaf {Γ}{A} t ⟧t ≡ leaf ⟦ t ⟧t
    ⟦node⟧ : ∀ {Γ A} {t t' : I.Tm Γ (I.Tree A)} →
      ⟦ I.node t t' ⟧t ≡ node ⟦ t ⟧t ⟦ t' ⟧t
    ⟦recTree⟧ : ∀{Γ A B}{u : I.Tm (Γ I.▹ A) B}{v : I.Tm (Γ I.▹ B I.▹ B) B}{t : I.Tm Γ (I.Tree A)} →
      ⟦ I.recTree u v t ⟧t ≡ recTree ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦leaf⟧ ⟦node⟧ ⟦recTree⟧ #-}

  ⟦def⟧ : ∀ {Γ A B} {t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl
\end{code}

\section{Examples}

\begin{code}[hide]
module Examples where
  open I
\end{code}
With the recursor we can define the following functions on natural
numbers. Addition with variable names is written
\verb$plus = λ x y . recNat y (z . suc z) x$
which means that we have a function with two inputs \verb$x$ and \verb$y$,
we do recursion on the first input (\verb$x$), and in the input we replace
\verb$zero$s by \verb$y$, and \verb$suc$s by \verb$suc$. The same information
is expressed in languages with pattern matching as follows.
\begin{verbatim}
plus zero      y = y
plus (suc x') y = suc (plus x' y)
\end{verbatim}
The first line corresponds to the first argument of \verb$recNat$ (i.e.\ \verb$y$),
the second line corresponds to the second argument in which \verb$z$ is bound
(\verb$z . suc z$) where \verb$z$ refers to the recursive call which we
write \verb$(plus x' y)$ using pattern matching notation.

In our formal
syntax we write this as follows.
\begin{code}
  plus : Tm ∙ (Nat ⇒ Nat ⇒ Nat)
  plus = lam (lam (recNat v0 (suc v0) v1))
\end{code}

The \verb$isZero$ operation can be defined as follows.
\begin{code}
  isZero : Tm ∙ (Nat ⇒ Bool)
  isZero = lam (recNat true false v0)
\end{code}
The zero case is simply \verb$true$, the successor case is \verb$false$
regardless of the result of the recursive call.

Examples for lists:
\begin{code}
  isnil : {A : Ty} → Tm ∙ (List A ⇒ Bool)
  isnil = lam (recList true false v0)

  concat : {A : Ty} → Tm ∙ (List A ⇒ List A ⇒ List A)
  concat = lam (lam (recList v0 (cons v1 v0) v1))
\end{code}
Concatenation works as follows.
\begin{code}
  concatTest : {A : Ty}{x y z : Tm ∙ A} →
    concat $ cons x (cons y nil) $ cons z nil ≡ cons x (cons y (cons z nil))
\end{code}
\begin{code}[hide]
  concatTest = refl
\end{code}
\verb$isnil$ and \verb$concat$ are polymorphic functions in the sense
that they work for any type \verb$A$ but the polymorphism happens in
our metalanguage and not in our object language.


The following two trees in \verb$Tm ∙ (Tree Nat)$

\begin{tikzpicture}
  \node (x10) at (0,0) {};
  \node (x20) at (-0.5,-1) {};
  \node (x21) at (0.5,-1) {\verb$zero$};
  \node (x30) at (-1,-2) {\verb$zero$};
  \node (x31) at (0,-2) {\verb$zero$};
  \draw[-] (x10) edge node {} (x20);
  \draw[-] (x10) edge node {} (x21);
  \draw[-] (x20) edge node {} (x30);
  \draw[-] (x20) edge node {} (x31);
  \node (y10) at (3,0) {};
  \node (y20) at (2.5,-1) {\verb$zero$};
  \node (y21) at (3.5,-1) {};
  \node (y30) at (3,-2) {\verb$zero$};
  \node (y31) at (4,-2) {\verb$zero$};
  \draw[-] (y10) edge node {} (y20);
  \draw[-] (y10) edge node {} (y21);
  \draw[-] (y21) edge node {} (y30);
  \draw[-] (y21) edge node {} (y31);
\end{tikzpicture}

are defined as
\begin{code}[hide]
  treeEx1 treeEx2 : Tm ∙ (Tree Nat)
\end{code}
\begin{code}
  treeEx1 = node (node (leaf zero) (leaf zero)) (leaf zero)
  treeEx2 = node (leaf zero) (node (leaf zero) (leaf zero))
\end{code}

The following function adds all the numbers in the leaves of a tree.
\begin{code}
  sum : Tm ∙ (Tree Nat ⇒ Nat)
  sum = lam (recTree v0 (plus [ p ∘ p ∘ p ] $ v1 $ v0) v0)
\end{code}
With variable names we would write this as
\verb@sum = λ x . recTree (z . z) (z₁ z₂ . plus $ z₁ $ z₂) x@.
In the formal syntax, we had to weaken \verb$plus$ by \verb$p ∘ p ∘ p$
because it was defined in the empty context above and now we used it
in a context with three free variables (one bound by \verb$lam$, the other
two by \verb$recTree$).

The following function turns a binary tree into a list:
\begin{code}
  flatten : ∀{A} → Tm ∙ (Tree A ⇒ List A)
  flatten = lam (recTree (cons v0 nil) (concat [ p ∘ p ∘ p ] $ v1 $ v0) v0)
\end{code}

\section{The standard algebra}

The new components of the standard algebra:
\begin{code}[hide]
St : Algebra
St = record
       { Con        = Set
       ; Sub        = λ Γ Δ → Γ → Δ
       ; Ty         = Set
       ; Tm         = λ Γ A → Γ → A
       ; _∘_        = _∘f_
       ; id         = idf
       ; ass        = refl
       ; idl        = refl
       ; idr        = refl
       ; _[_]       = _∘f_
       ; [id]       = refl
       ; [∘]        = refl
       ; ∙          = ↑p 𝟙
       ; ε          = const *↑
       ; ∙η         = refl
       ; _▹_        = _⊗_
       ; _,_        = λ σ t γ → σ γ ,Σ t γ
       ; p          = π₁
       ; q          = π₂
       ; ▹β₁        = refl
       ; ▹β₂        = refl
       ; ▹η         = refl

       ; Bool       = 𝟚
       ; true       = const I
       ; false      = const O
       ; ite        = λ b t f γ → if b γ then t γ else f γ
       ; Boolβ₁     = refl
       ; Boolβ₂     = refl
       ; true[]     = refl
       ; false[]    = refl
       ; ite[]      = refl
\end{code}
\begin{code}
       ; Nat        = ℕ
       ; zero       = const O
       ; suc        = S ∘f_
       ; recNat     = λ z s n γ → rec-ℕ (z γ) (λ n → s (γ ,Σ n)) (n γ)
       ; Natβ₁      = refl
       ; Natβ₂      = refl
       ; zero[]     = refl
       ; suc[]      = refl
       ; recNat[]   = refl
\end{code}
\begin{code}[hide]
       ; _⇒_        = λ A B → A → B
       ; lam        = λ t γ a → t (γ ,Σ a)
       ; app        = λ { t (γ ,Σ a) → t γ a }
       ; ⇒β         = refl
       ; ⇒η         = refl
       ; lam[]      = refl
       
       ; Unit       = ↑p 𝟙
       ; tt         = const *↑
       ; Unitη      = refl
       
       ; _×_        = _⊗_
       ; ⟨_,_⟩      = λ a b γ → (a γ ,Σ b γ)
       ; proj₁      = λ t γ → π₁ (t γ)
       ; proj₂      = λ t γ → π₂ (t γ)
       ; ×β₁        = refl
       ; ×β₂        = refl
       ; ×η         = refl
       ; ⟨,⟩[]      = refl
\end{code}
\begin{code}
       ; List       = λ A → ⁅ A ⁆
       ; nil        = λ Γ → ⁅⁆
       ; cons       = λ t ts γ → t γ ∷ ts γ
       ; recList    = λ b f l γ → rec-⁅⁆ (b γ) (λ a b → f (γ ,Σ b ,Σ a)) (l γ)
       ; Listβ₁     = refl
       ; Listβ₂     = refl
       ; nil[]      = refl
       ; cons[]     = refl
       ; recList[]  = refl

       ; Tree      = 𝕋3
       ; leaf      = λ a γ → Leaf (a γ)
       ; node      = λ ll rr γ → Node (ll γ) (rr γ)
       ; recTree   = λ l n t γ → rec-𝕋3 (λ a → l (γ ,Σ a)) (λ ll rr → n (γ ,Σ ll ,Σ rr)) (t γ)
       ; Treeβ₁    = refl
       ; Treeβ₂    = refl
       ; leaf[]    = refl
       ; node[]    = refl
       ; recTree[] = refl
\end{code}
\begin{code}[hide]
       }
\end{code}

\begin{code}[hide]
record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×_

  field
    Con : I.Con → Set i
    Ty  : I.Ty → Set j
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Tm  : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ

    _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])
    [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
      {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ∙ : Con I.∙
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ cong (Sub Γ ∙) I.∙η ]= ε

    _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')
    _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
    p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p
    q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
    ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ

    Bool : Ty I.Bool
    true : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.true
    false : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.false
    ite : ∀ {Γ' A' b' t' f'}  {Γ : Con Γ'}{A : Ty A'} →
      Tm Γ Bool b' → Tm Γ A t' → Tm Γ A f' → Tm Γ A (I.ite b' t' f')
    true[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      true [ σ ] ≡ true
    false[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      false [ σ ] ≡ false
    ite[] : ∀ {Γ' Δ' A' b' t' f' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {b : Tm Δ Bool b'}{t : Tm Δ A t'}{f : Tm Δ A f'}
      {σ : Sub Γ Δ σ'} →
      (ite b t f) [ σ ] ≡ ite (b [ σ ]) (t [ σ ]) (f [ σ ])
    iteβ₁ : ∀ {Γ' A' t' f'} {Γ : Con Γ'}{A : Ty A'}
      {t : Tm Γ A t'}{f : Tm Γ A f'} → ite true t f ≡ t
    iteβ₂ : ∀ {Γ' A' t' f'} {Γ : Con Γ'}{A : Ty A'}
      {t : Tm Γ A t'}{f : Tm Γ A f'} → ite false t f ≡ f

    Nat        : Ty I.Nat
    zero       : ∀{Γ'}{Γ : Con Γ'} → Tm Γ Nat I.zero
    suc        : ∀{Γ' n'}{Γ : Con Γ'} →
                 Tm Γ Nat n' → Tm Γ Nat (I.suc n')
    recNat     : ∀{Γ' A' z' s' t'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ A z' → Tm (Γ ▹ A) A s' → Tm Γ Nat t' → Tm Γ A (I.recNat z' s' t')
    Natβ₁      : ∀{Γ' A' z' s'}{Γ : Con Γ'}{A : Ty A'}{z : Tm Γ A z'}{s : Tm (Γ ▹ A) A s'} → recNat z s zero ≡ z
    Natβ₂      : ∀{Γ' A' z' s' t'}{Γ : Con Γ'}{A : Ty A'}{z : Tm Γ A z'}{s : Tm (Γ ▹ A) A s'}{t : Tm Γ Nat t'} → recNat z s (suc t) ≡ s [ id , recNat z s t ]
    zero[]     : ∀{Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
                 zero [ σ ] ≡ zero
    suc[]      : ∀{Γ' Δ' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{n : Tm Δ Nat n'}{σ : Sub Γ Δ σ'} →
                 (suc n) [ σ ] ≡ suc (n [ σ ])
    recNat[]   : ∀{Γ' Δ' A' z' s' t' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{z : Tm Δ A z'}{s : Tm (Δ ▹ A) A s'}{t : Tm Δ Nat t'}{σ : Sub Γ Δ σ'} →
                 recNat z s t [ σ ] ≡ recNat (z [ σ ]) (s [ σ ∘ p , q ]) (t [ σ ])

    _⇒_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.⇒ B')
    lam : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm (Γ ▹ A) B t' → Tm Γ (A ⇒ B) (I.lam t')
    app : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A ⇒ B) t' → Tm (Γ ▹ A) B (I.app t')
    ⇒β : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Γ ▹ A) B t'} → app (lam t) ≡ t
    ⇒η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A ⇒ B) t'} → lam (app t) ≡ t
    lam[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Δ ▹ A) B t'}{σ : Sub Γ Δ σ'} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])
      
    Unit : Ty I.Unit
    tt : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Unit I.tt
    Unitη : ∀ {Γ' t'} {Γ : Con Γ'} {t : Tm Γ Unit t'} →
      t =[ cong (Tm Γ Unit) I.Unitη ]= tt

    _×_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.× B')
    ⟨_,_⟩ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ A u' → Tm Γ B v' → Tm Γ (A × B) I.⟨ u' , v' ⟩
    proj₁ : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A × B) t' → Tm Γ A (I.proj₁ t')
    proj₂ : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A × B) t' → Tm Γ B (I.proj₂ t')
    ×β₁ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Γ A u'}{v : Tm Γ B v'} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Γ A u'}{v : Tm Γ B v'} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A × B) t'} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
    ⟨,⟩[] : ∀ {Γ' Δ' A' B' u' v' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Δ A u'}{v : Tm Δ B v'}{σ : Sub Γ Δ σ'} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩
      
    List       : ∀{A'} → Ty A' → Ty (I.List A')
    nil        : ∀{Γ' A'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ (List A) I.nil
    cons       : ∀{Γ' A' a' as'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ A a' → Tm Γ (List A) as' → Tm Γ (List A) (I.cons a' as')
    recList    : ∀{Γ' A' B' e' f' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} → Tm Γ B e' → Tm (Γ ▹ A ▹ B) B f'  → Tm Γ (List A) t' → Tm Γ B (I.recList e' f' t')
    Listβ₁     : ∀{Γ' A' B' e' f'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{e : Tm Γ B e'}{f : Tm (Γ ▹ A ▹ B) B f'} → recList e f nil ≡ e
    Listβ₂     : ∀{Γ' A' B' e' f' a' as'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{e : Tm Γ B e'}{f : Tm (Γ ▹ A ▹ B) B f'}{a : Tm Γ A a'}{as : Tm Γ (List A) as'} →
                 recList e f (cons a as) ≡ (f [ id , a , recList e f as ])
    nil[]      : ∀{Γ' Δ' A' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{σ : Sub Γ Δ σ'} → nil {Γ = Δ}{A = A} [ σ ] ≡ nil
    cons[]     : ∀{Γ' Δ' A' a' as' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{as : Tm Δ (List A) as'}{a : Tm Δ A a'}{σ : Sub Γ Δ σ'} →
                 (cons a as) [ σ ] ≡ cons (a [ σ ]) (as [ σ ])
    recList[]  : ∀{Γ' Δ' A' B' u' v' as' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}{u : Tm Δ B u'}{v : Tm (Δ ▹ A ▹ B) B v'}{as : Tm Δ (List A) as'}{σ : Sub Γ Δ σ'} →
                 recList u v as [ σ ] ≡ recList (u [ σ ]) (v [ (σ ∘ p , q) ∘ p , q ]) (as [ σ ])
                 
    Tree       : ∀{A} → Ty A → Ty (I.Tree A)
    leaf       : ∀{Γ' A' t'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ A t' → Tm Γ (Tree A) (I.leaf t')
    node       : ∀{Γ' A' ll' rr'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ (Tree A) ll' → Tm Γ (Tree A) rr' → Tm Γ (Tree A) (I.node ll' rr')
    recTree    : ∀{Γ' A' B' l' n' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} → Tm (Γ ▹ A) B l' → Tm (Γ ▹ B ▹ B) B n' → Tm Γ (Tree A) t' → Tm Γ B (I.recTree l' n' t')
    Treeβ₁     : ∀{Γ' A' B' l' n' a'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{l : Tm (Γ ▹ A) B l'}{n : Tm (Γ ▹ B ▹ B) B n'}{a : Tm Γ A a'} → recTree l n (leaf a) ≡ l [ id , a ]
    Treeβ₂     : ∀{Γ' A' B' l' n' ll' rr'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{l : Tm (Γ ▹ A) B l'}{n : Tm (Γ ▹ B ▹ B) B n'}{ll : Tm Γ (Tree A) ll'}{rr : Tm Γ (Tree A) rr'} →
                  recTree l n (node ll rr) ≡ n [ id , recTree l n ll , recTree l n rr ]
    leaf[]     : ∀{Γ' Δ' A' a' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{a : Tm Δ A a'}{σ : Sub Γ Δ σ'} → leaf {A = A} a [ σ ] ≡ leaf (a [ σ ])
    node[]     : ∀{Γ' Δ' A' ll' rr' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{ll : Tm Δ (Tree A) ll'}{rr : Tm Δ (Tree A) rr'}{σ : Sub Γ Δ σ'} →
                  (node ll rr) [ σ ] ≡ node (ll [ σ ]) (rr [ σ ])
    recTree[]  : ∀{Γ' Δ' A' B' l' n' t' σ'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{Δ : Con Δ'}{l : Tm (Δ ▹ A) B l'}{n : Tm (Δ ▹ B ▹ B) B n'}{t : Tm Δ (Tree A) t'}{σ : Sub Γ Δ σ'} →
                  recTree l n t [ σ ] ≡ recTree (l [ (σ ∘ p) , q ]) (n [ (σ ∘ p ∘ p) , (q [ p ]) , q ]) (t [ σ ])

  def : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ A t' → Tm (Γ ▹ A) B u' → Tm Γ B (I.def t' u')
  def t u = u [ id , t ]

  _$_ : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ (A ⇒ B) t' → Tm Γ A u' → Tm Γ B (t' I.$ u')
  t $ u = def u (app t)

  -------------------------------------------
  -- eliminator
  -------------------------------------------

  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool
  ⟦ I.List A ⟧T = List ⟦ A ⟧T
  ⟦ I.Tree A ⟧T = Tree ⟦ A ⟧T
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ I.Unit ⟧T = Unit
  ⟦ A I.× B ⟧T = ⟦ A ⟧T × ⟦ B ⟧T

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t

    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}

    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦recNat⟧ : ∀{Γ A}{u : I.Tm Γ A}{v : I.Tm (Γ I.▹ A) A}{t : I.Tm Γ I.Nat} →
      ⟦ I.recNat u v t ⟧t ≡ recNat ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦recNat⟧ #-}

    ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
    ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
    ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ I.Bool}{u v : I.Tm Γ A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}
    
    ⟦tt⟧ : ∀ {Γ} → ⟦ I.tt {Γ} ⟧t ≡ tt
    {-# REWRITE ⟦tt⟧ #-}

    ⟦⟨,⟩⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.⟨ u , v ⟩ ⟧t ≡ ⟨ ⟦ u ⟧t , ⟦ v ⟧t ⟩
    ⟦proj₁⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₁ t ⟧t ≡ proj₁ ⟦ t ⟧t
    ⟦proj₂⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₂ t ⟧t ≡ proj₂ ⟦ t ⟧t
    {-# REWRITE ⟦⟨,⟩⟧ ⟦proj₁⟧ ⟦proj₂⟧ #-}
    
    ⟦nil⟧ : ∀ {Γ A} → ⟦ I.nil {Γ}{A} ⟧t ≡ nil
    ⟦cons⟧ : ∀ {Γ A}{a : I.Tm Γ A}{as : I.Tm Γ (I.List A)} →
      ⟦ I.cons a as ⟧t ≡ cons ⟦ a ⟧t ⟦ as ⟧t
    ⟦recList⟧ : ∀{Γ A B}{e : I.Tm Γ B}{f : I.Tm (Γ I.▹ A I.▹ B) B}{as : I.Tm Γ (I.List A)} →
      ⟦ I.recList e f as ⟧t ≡ recList ⟦ e ⟧t ⟦ f ⟧t ⟦ as ⟧t
    {-# REWRITE ⟦nil⟧ ⟦cons⟧ ⟦recList⟧ #-}
    
    ⟦leaf⟧ : ∀ {Γ A}{t : I.Tm Γ A} → ⟦ I.leaf {Γ}{A} t ⟧t ≡ leaf ⟦ t ⟧t
    ⟦node⟧ : ∀ {Γ A} {t t' : I.Tm Γ (I.Tree A)} →
      ⟦ I.node t t' ⟧t ≡ node ⟦ t ⟧t ⟦ t' ⟧t
    ⟦recTree⟧ : ∀{Γ A B}{u : I.Tm (Γ I.▹ A) B}{v : I.Tm (Γ I.▹ B I.▹ B) B}{t : I.Tm Γ (I.Tree A)} →
      ⟦ I.recTree u v t ⟧t ≡ recTree ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦leaf⟧ ⟦node⟧ ⟦recTree⟧ #-}

  ⟦def⟧ : ∀ {Γ A B}{t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl
\end{code}

\section{Canonicity}

\begin{code}[hide]
open I

-- Bool

data PBool : Tm ∙ Bool → Set where
  Ptrue  : PBool true
  Pfalse : PBool false

Pite :
  {A' : Ty}{t' : Tm ∙ Bool}{u' v' : Tm ∙ A'}(A : Tm ∙ A' → Set) →
  PBool t' → A u' → A v' → A (ite t' u' v')
Pite {u' = u'} {v'} A Ptrue  u v = u
Pite {u' = u'} {v'} A Pfalse u v = v

PcanBool : ∀{t'} → PBool t' → ↑p (t' ≡ true) ⊎ ↑p (t' ≡ false)
PcanBool Ptrue  = ι₁ ↑[ refl ]↑
PcanBool Pfalse = ι₂ ↑[ refl ]↑

-- Nat

\end{code}

To prove canonicity for natural numbers as, the predicate for natural
numbers \verb$PNat$ is given inductively saying that the preicate
holds for \verb$zero$ and if it holds for a number, it holds for its
successor.
\begin{code}
data PNat : Tm ∙ Nat → Set where
  Pzero : PNat zero
  Psuc : ∀{n'} → PNat n' → PNat (suc n')
\end{code}
To define \verb$recNat$ in the canonicity dependent algebra, we will need
that if the terms \verb$u'$ and \verb$v'$ preserve the predicate for a type
\verb$A'$, then if \verb$PNat$ holds for \verb$t'$, then the predicate for \verb$A'$
holds for \verb$recNat u' v' t'$:
\begin{code}
PrecNat :
  {A' : Ty}{u' : Tm ∙ A'}{v' : Tm (∙ ▹ A') A'}{t' : Tm ∙ Nat}
  (A : Tm ∙ A' → Set)(u : A u')(v : ∀{w'} → A w' → A (v' [ id , w' ]))
  (t : PNat t') →
  A (recNat u' v' t')
PrecNat {A'}{u'}{v'}{.zero}    A u v Pzero    = u
PrecNat {A'}{u'}{v'}{.(suc _)} A u v (Psuc t) = v (PrecNat A u v t)
\end{code}
If the predicate \verb$PNat$ holds for a term, it is either zero or the successor of
another term.
\begin{code}
PcanNat : ∀{t'} → PNat t' → ↑p (t' ≡ zero) ⊎ Σ (Tm ∙ Nat) λ u → ↑p (t' ≡ suc u)
PcanNat Pzero    = ι₁ ↑[ refl ]↑
PcanNat (Psuc t) with PcanNat t
... | ι₁ e = ι₂ (zero ,Σ ↑[ ap suc ↓[ e ]↓ ]↑)
... | ι₂ (t' ,Σ e) = ι₂ (suc t' ,Σ ↑[ ap suc ↓[ e ]↓ ]↑)
\end{code}

\begin{code}[hide]

-- List

\end{code}
We have a similar predicate for lists and a function which shows that the recursor
preserves this predicate.
\begin{code}
data PList {A'}(A : Tm ∙ A' → Set) : Tm ∙ (List A') → Set where
  Pnil : PList A nil
  Pcons : ∀{u' t'} → A u' → PList A t' → PList A (cons u' t')

PrecList :
  {A' B' : Ty}{u' : Tm ∙ B'}{v' : Tm (∙ ▹ A' ▹ B') B'}{t' : Tm ∙ (List A')}
  {A : Tm ∙ A' → Set}(B : Tm ∙ B' → Set)(u : B u')(v : ∀{w₁' w₂'} → A w₁' → B w₂' → B (v' [ id , w₁' , w₂' ]))(t : PList A t') →
  B (recList u' v' t')
PrecList B u v Pnil = u
PrecList B u v (Pcons w t) = v w (PrecList B u v t)
\end{code}
Canonicity for lists follows from the predicate;
\begin{code}
PcanList :
  {A' : Ty} → {t' : Tm ∙ (List A')} → {A : Tm ∙ A' → Set} → PList A t' →
  ↑p (t' ≡ nil) ⊎ Σ (Tm ∙ A') λ a' → Σ (Tm ∙ (List A')) λ as' → ↑p (t' ≡ cons a' as')
PcanList Pnil = ι₁ refl↑
PcanList (Pcons {a'} {as'} a as) = ι₂ (a' ,Σ (as' ,Σ refl↑))
\end{code}
\begin{code}[hide]

-- Tree

\end{code}
The same for trees:
\begin{code}
data PTree {A' : Ty} (A : Tm ∙ A' → Set) : Tm ∙ (Tree A') → Set where
  Pleaf : {a' : Tm ∙ A'} → A a' → PTree A (leaf a')
  Pnode : {ll' rr' : Tm ∙ (Tree A')} →
           PTree A ll' → PTree A rr' → PTree A (node ll' rr')

PrecTree :
  {A' B' : Ty} → {l' : Tm (∙ ▹ A') B'} → {n' : Tm (∙ ▹ B' ▹ B') B'} → {t' : Tm ∙ (Tree A')} →
  {A : Tm ∙ A' → Set} → (B : Tm ∙ B' → Set) →
  (l : {a' : Tm ∙ A'} → A a' → B (l' [ id , a' ])) →
  (n : {ll rr : Tm ∙ B'} → B ll → B rr → B (n' [ id , ll , rr ])) →
  (t : PTree A t') → B (recTree l' n' t')
PrecTree B l n (Pleaf a) = l a
PrecTree B l n (Pnode ll rr) = n (PrecTree B l n ll) (PrecTree B l n rr)

PcanTree :
  {A' : Ty} → {t' : Tm ∙ (Tree A')} → {A : Tm ∙ A' → Set} → PTree A t' →
  (Σ (Tm ∙ A') λ a' → ↑p (t' ≡ leaf a')) ⊎
  Σ (Tm ∙ (Tree A')) λ ll' → Σ (Tm ∙ (Tree A')) λ rr' → ↑p (t' ≡ node ll' rr')
PcanTree (Pleaf {a'} a) = ι₁ (a' ,Σ refl↑)
PcanTree (Pnode {ll'} {rr'} ll rr) = ι₂ (ll' ,Σ (rr' ,Σ refl↑))
\end{code}
Now we can define the new components of the canonicity dependent algebra:
\begin{code}
Can : DepAlgebra
Can = record
       { Con        = λ Γ' → Sub ∙ Γ' → Set
       ; Sub        = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
       ; Ty         = λ A' → Tm ∙ A' → Set
       ; Tm         = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
       ; _∘_        = λ σ δ x → σ (δ x)
       ; id         = idf
       ; ass        = refl
       ; idl        = refl
       ; idr        = refl
       ; _[_]       = λ t σ x → t (σ x)
       ; [id]       = refl
       ; [∘]        = refl
       ; ∙          = λ _ → ↑p 𝟙
       ; ε          = λ _ → *↑
       ; ∙η         = refl
       ; _▹_        = λ Γ A σ' → Γ (p ∘ σ') ⊗ A (q [ σ' ])
       ; _,_        = λ σ t x → σ x ,Σ t x
       ; p          = π₁
       ; q          = π₂
       ; ▹β₁        = refl
       ; ▹β₂        = refl
       ; ▹η         = refl
\end{code}
\begin{code}[hide]
       ; Bool       = PBool
       ; true       = λ _ → Ptrue
       ; false      = λ _ → Pfalse
       ; ite        = λ where {A = A} t u v {ν'} ν̂ → Pite A (t ν̂) (u ν̂) (v ν̂)
       ; iteβ₁      = refl
       ; iteβ₂      = refl
       ; true[]     = refl
       ; false[]    = refl
       ; ite[]      = refl
\end{code}
\begin{code}
       ; Nat        = PNat
       ; zero       = λ _ → Pzero
       ; suc        = λ t ν̂ → Psuc (t ν̂)
       ; recNat     = λ where {A = A} u v t {ν'} ν̂ → PrecNat A (u ν̂) (λ ŵ → v (ν̂ ,Σ ŵ)) (t ν̂)
       ; Natβ₁      = refl
       ; Natβ₂      = refl
       ; zero[]     = refl
       ; suc[]      = refl
       ; recNat[]   = refl
\end{code}
\begin{code}[hide]
       ; _⇒_        = λ {A'}{B'} A B t' → (u' : Tm ∙ A') → A u' → B (t' $ u')
       ; lam        = λ t {ν'} ν̂ u' û → t {ν' , u'} (ν̂ ,Σ û)
       ; app        = λ t {ν'} ν̂ → t {p ∘ ν'} (π₁ ν̂) (q [ ν' ]) (π₂ ν̂)
       ; ⇒β         = refl
       ; ⇒η         = refl
       ; lam[]      = refl

       ; Unit       = λ _ → ↑p 𝟙
       ; tt         = _
       ; Unitη      = refl

       ; _×_        = λ A B t' → A (proj₁ t') ⊗ B (proj₂ t')
       ; ⟨_,_⟩      = λ u v ν̂ → u ν̂ ,Σ v ν̂
       ; proj₁      = λ t ν̂ → π₁ (t ν̂)
       ; proj₂      = λ t ν̂ → π₂ (t ν̂)
       ; ×β₁        = refl
       ; ×β₂        = refl
       ; ×η         = refl
       ; ⟨,⟩[]      = refl
\end{code}
\begin{code}
       ; List       = PList
       ; nil        = λ _ → Pnil
       ; cons       = λ u t ν̂ → Pcons (u ν̂) (t ν̂)
       ; recList    = λ where {B = B} u v t ν̂ → PrecList B (u ν̂) (λ w₁ w₂ → v (ν̂ ,Σ w₁ ,Σ w₂)) (t ν̂)
       ; Listβ₁     = refl
       ; Listβ₂     = refl
       ; nil[]      = refl
       ; cons[]     = refl
       ; recList[]  = refl

       ; Tree       = PTree
       ; leaf       = λ a ν̂ → Pleaf (a ν̂)
       ; node       = λ ll rr ν̂ → Pnode (ll ν̂) (rr ν̂)
       ; recTree    = λ where {B = B} l n t ν̂ → PrecTree B (λ b → l (ν̂  ,Σ b)) (λ ll rr → n (ν̂ ,Σ ll ,Σ rr)) (t ν̂ )
       ; Treeβ₁     = refl
       ; Treeβ₂     = refl
       ; leaf[]     = refl
       ; node[]     = refl
       ; recTree[]  = refl
\end{code}
\begin{code}[hide]
       }
module Can = DepAlgebra Can
\end{code}
Interpretating into the canonicity dependent algebra gives actual canonicity for
terms:
\begin{code}
canNat : (t : Tm ∙ Nat) → ↑p (t ≡ zero) ⊎ Σ (Tm ∙ Nat) λ u → ↑p (t ≡ suc u)
canNat t = PcanNat (Can.⟦ t ⟧t {id} ↑[ * ]↑)

canList : ∀{A}(t : Tm ∙ (List A)) →
  ↑p (t ≡ nil) ⊎ Σ (Tm ∙ A) λ u → Σ (Tm ∙ (List A)) λ v → ↑p (t ≡ cons u v)
canList t = PcanList (Can.⟦ t ⟧t {id} *↑)

canTree : ∀{A}(t : Tm ∙ (Tree A)) →
  (Σ (Tm ∙ A) λ a → ↑p (t ≡ leaf a)) ⊎
  Σ (Tm ∙ (Tree A)) λ ll → Σ (Tm ∙ (Tree A)) λ rr → ↑p (t ≡ node ll rr)
canTree t = PcanTree (Can.⟦ t ⟧t {id} *↑)
\end{code}
