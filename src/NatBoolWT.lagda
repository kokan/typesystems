\section{Well-typed syntax}

The abstract syntax tree description of NatBool still contains
meaningless programs, e.g.\ \verb$isZero true$ or \verb$true + zero$.
The problem is that \verb$isZero$ expects a natural number and
not a boolean and \verb$+$ expects two natural numbers. The solution
is to add the type information to the terms, that is, whether a term
is a boolean or a natural number.

NatBoolWT (well-typed) contains two sorts: \verb$Ty$ and \verb$Tm$,
and the latter sort is indexed by the first one. 
\begin{code}[hide]
{-# OPTIONS --prop #-}
module NatBoolWT where

open import Lib

module I where
  data Ty   : Set where
    Nat     : Ty
    Bool    : Ty
  data Tm   : Ty → Set where
    zero    : Tm Nat
    suc     : Tm Nat → Tm Nat
    isZero  : Tm Nat → Tm Bool
    _+_     : Tm Nat → Tm Nat → Tm Nat
    true    : Tm Bool
    false   : Tm Bool
    ite     : ∀{A} → Tm Bool → Tm A → Tm A → Tm A
\end{code}
A NatBoolWT algebra is a record with the following components.
\begin{code}
record Algebra {i j} : Set (lsuc (i ⊔ j)) where
  field
    Ty      : Set i
    Tm      : Ty → Set j
    Nat     : Ty
    Bool    : Ty
    zero    : Tm Nat
    suc     : Tm Nat → Tm Nat
    isZero  : Tm Nat → Tm Bool
    _+_     : Tm Nat → Tm Nat → Tm Nat
    true    : Tm Bool
    false   : Tm Bool
    ite     : ∀ {A} → Tm Bool → Tm A → Tm A → Tm A
\end{code}

There is no set of terms anymore, but there is a set of
\verb$Nat$-terms and a set of \verb$Bool$-terms,
separately. Each operator expects a term of the correct type as input
and returns a term of the appropriate
type. \verb$ite$ expects two terms of type \verb$A$ and returns a term of type \verb$A$ where
\verb$A : Ty$. This \verb$A$ is an \emph{implicit argument} of
\verb$ite$ which we omit for readability (the full type would otherwise be
\verb$(A : Ty) → Tm Bool → Tm A → Tm A → Tm A$).

We could have used two sorts, \verb$TmBool$ and \verb$TmNat$ instead
however that would not generalise to function types (Chapter
\ref{TODO}). 

The syntax of NatBoolWT is still called \verb$I$ and its recursor
consists of two functions (one for each sort, denoted by
\verb$⟦_⟧T$ and \verb$⟦_⟧t$) which preserve all operations.
\begin{code}
  ⟦_⟧T  : I.Ty → Ty
  ⟦_⟧t  : ∀{A} → I.Tm A → Tm ⟦ A ⟧T
  ⟦ I.Nat           ⟧T = Nat
  ⟦ I.Bool          ⟧T = Bool
  ⟦ I.true          ⟧t = true
  ⟦ I.false         ⟧t = false
  ⟦ I.ite t t' t''  ⟧t = ite ⟦ t ⟧t ⟦ t' ⟧t ⟦ t'' ⟧t
  ⟦ I.zero          ⟧t = zero
  ⟦ I.suc t         ⟧t = suc ⟦ t ⟧t
  ⟦ I.isZero t      ⟧t = isZero ⟦ t ⟧t
  ⟦ t I.+ t'        ⟧t = ⟦ t ⟧t + ⟦ t' ⟧t
\end{code}

\begin{exe}
  Implement the size, height and the number of \verb$true$s functions for NatBoolWT, show that \verb$trues t ≤ 3 ^ℕ height t$. See the previous section.
\end{exe}

\subsection{Type inference}

Using the NatBoolWT syntax $\I$, we define the following
NatBoolAST algebra.
\begin{code}[hide]
import NatBoolAST
\end{code}
\begin{code}
_≟T_ : (A A' : I.Ty) → (↑p (A ≡ A')) ⊎ (↑p (¬ (A ≡ A')))
I.Nat   ≟T I.Nat   = ι₁ ↑[ refl ]↑
I.Nat   ≟T I.Bool  = ι₂ ↑[ (λ ()) ]↑
I.Bool  ≟T I.Bool  = ι₁ ↑[ refl ]↑
I.Bool  ≟T I.Nat   = ι₂ ↑[ (λ ()) ]↑

Inf-ite : Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm) →
  Maybe (Σ I.Ty I.Tm)
Inf-ite (Just (I.Bool , t)) (Just (A , t')) (Just (A' , t'')) =
  ind⊎ _  (λ e → Just (A , I.ite t t' (transport I.Tm ↓[ e ]↓ t'')))
          (λ _ → Nothing)
          (A' ≟T A)
Inf-ite _ _ _ = Nothing

Inf-+ : Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm)
Inf-+ (Just (I.Nat , t)) (Just (I.Nat , t')) = Just (I.Nat , t I.+ t')
Inf-+ _ _ = Nothing

Inf : NatBoolAST.Algebra
Inf = record
  { Tm      = Maybe (Σ I.Ty I.Tm)
  ; true    = Just (I.Bool , I.true)
  ; false   = Just (I.Bool , I.false)
  ; ite     = Inf-ite
  ; zero    = Just (I.Nat , I.zero)
  ; suc     = λ { (Just (I.Nat , t)) → Just (I.Nat   , I.suc t)     ; _ → Nothing }
  ; isZero  = λ { (Just (I.Nat , t)) → Just (I.Bool  , I.isZero t)  ; _ → Nothing }
  ; _+_     = Inf-+
  }
module Inf = NatBoolAST.Algebra Inf
\end{code}
Terms in this NatBoolAST algebra are either a syntactic type and a
term of that type or failure. We used pattern matching on syntactic
types.

Interpretation into this algebra gives us type inference.
\begin{code}
infer : NatBoolAST.I.Tm → Maybe (Σ I.Ty I.Tm)
infer = Inf.⟦_⟧
\end{code}

\subsection{Standard interpretation}

We define the standard algebra (set algebra, denotational
semantics). The idea is that all operators in our object language are
given by their metatheoretic counterparts.
\begin{code}
isO : ℕ → 𝟚
isO O = I
isO (S n) = O

St : Algebra
St = record
  { Ty = Set
  ; Tm = λ T → T
  ; Nat = ℕ
  ; Bool = 𝟚
  ; zero = O
  ; suc = S
  ; isZero = isO
  ; _+_ = _+ℕ_
  ; true = I
  ; false = O
  ; ite = if_then_else_
  }
module St = Algebra St
\end{code}
Using the recursor we get an intepreter (the metacircular interpreter, normaliser) for our language:
\begin{code}
norm : ∀{A} → I.Tm A → St.⟦ A ⟧T
norm = St.⟦_⟧t
\end{code}

We are not able to define the standard interpreter for NatBoolAST
because there we had meaningless programs. For example, we would have
had to interpret both \verb$Bool$ and \verb$Nat$ by the same set.

\subsection{Typing relation}

The traditional way of defining the well-typed syntax is by defining a
binary relation between syntactic types and syntactic NatBoolAST
terms. This relation is defined inductively as follows.
\begin{code}
module I' = NatBoolAST.I
data _⦂_  :  I'.Tm → I.Ty → Prop where
  zero    :  
                               -----------------------
                               I'.zero ⦂ I.Nat
  suc     :  ∀{t} →
                               t ⦂ I.Nat                 →
                               -----------------------
                               I'.suc t ⦂ I.Nat
  isZero  :  ∀{t} →
                               t ⦂ I.Nat                 →
                               -----------------------
                               I'.isZero t ⦂ I.Bool
  _+_     :  ∀{t t'} →
                               t ⦂ I.Nat                 →
                               t' ⦂ I.Nat                →
                               -----------------------
                               (t I'.+ t') ⦂ I.Nat
  true    :
                               -----------------------
                               I'.true ⦂ I.Bool
  false   :
                               -----------------------
                               I'.false ⦂ I.Bool
  ite     :  ∀{t t' t'' A} →
                               t ⦂ I.Bool                →
                               t' ⦂ A                    →
                               t'' ⦂ A                   →
                               -----------------------
                               I'.ite t t' t'' ⦂ A
\end{code}
\begin{exe}
  Show that there is an isomorphism between \verb$I.Tm A$ and \verb$Σ I'.Tm (_⦂ A)$.
\end{exe}

We defined well-typed terms directly because we do not want to talk
about non well-typed terms in the same way as we are not interested in
programs with non-matching brackets.
