\section{Well-typed syntax}

As a warmup for the real description of the language Def, we look at
its well-typed description which does not include equations.

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module DefWT where

open import Lib hiding (_∘_)
module I where
  data Ty   : Set where
    Nat     : Ty
    Bool    : Ty
    
  data Con : Set where
    ∙ : Con
    _▹_ : Con → Ty → Con

  _++_ : Con → Con → Con
  Γ ++ ∙ = Γ
  Γ ++ (Δ ▹ A) = (Γ ++ Δ) ▹ A

  infixl 5 _▹_

  data Var : Con → Ty → Set where
    vz : ∀{Γ A} → Var (Γ ▹ A) A
    vs : ∀{Γ A B} → Var Γ A → Var (Γ ▹ B) A

  data Tm (Γ : Con) : Ty → Set where
    var     : ∀{A} → Var Γ A → Tm Γ A
    def     : ∀{A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B

    zero    : Tm Γ Nat
    suc     : Tm Γ Nat → Tm Γ Nat
    isZero  : Tm Γ Nat → Tm Γ Bool
    _+_     : Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
    true    : Tm Γ Bool
    false   : Tm Γ Bool
    ite     : ∀{A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
\end{code}
Some abbreviations:
\begin{code}
  v0 : {Γ : Con} → {A : Ty}        → Tm (Γ ▹ A) A
  v0 = var vz
  v1 : {Γ : Con} → {A B : Ty}      → Tm (Γ ▹ A ▹ B) A
  v1 = var (vs vz)
  v2 : {Γ : Con} → {A B C : Ty}    → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = var (vs (vs vz))
  v3 : {Γ : Con} → {A B C D : Ty}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = var (vs (vs (vs vz)))
\end{code}
A Def-wt algebra has the following components.
\begin{code}
record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  field
    Ty      : Set i
    Nat     : Ty
    Bool    : Ty
    
    Con     : Set j
    ∙       : Con
    _▹_     : Con → Ty → Con

    Var     : Con → Ty → Set k
    vz      : ∀{Γ A} → Var (Γ ▹ A) A
    vs      : ∀{Γ A B} → Var Γ A → Var (Γ ▹ B) A

    Tm      : Con → Ty → Set l
    var     : ∀{Γ A} → Var Γ A → Tm Γ A
    def     : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B

    zero    : ∀{Γ} → Tm Γ Nat
    suc     : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    isZero  : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
    _+_     : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
    true    : ∀{Γ} → Tm Γ Bool
    false   : ∀{Γ} → Tm Γ Bool
    ite     : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
\end{code}
\begin{code}[hide]
  ⟦_⟧T : I.Ty → Ty
  ⟦ I.Nat   ⟧T = Nat
  ⟦ I.Bool  ⟧T = Bool

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙      ⟧C = ∙
  ⟦ Γ I.▹ A  ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  ⟦_⟧v : ∀{Γ A} → I.Var Γ A → Var ⟦ Γ ⟧C ⟦ A ⟧T
  ⟦ I.vz    ⟧v = vz
  ⟦ I.vs x  ⟧v = vs ⟦ x ⟧v

  ⟦_⟧t : ∀{Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T
  ⟦ I.var x         ⟧t = var ⟦ x ⟧v
  ⟦ I.def t t'      ⟧t = def ⟦ t ⟧t ⟦ t' ⟧t
  ⟦ I.zero          ⟧t = zero
  ⟦ I.suc t         ⟧t = suc ⟦ t ⟧t
  ⟦ I.isZero t      ⟧t = isZero ⟦ t ⟧t
  ⟦ t I.+ t'        ⟧t = ⟦ t ⟧t + ⟦ t' ⟧t
  ⟦ I.true          ⟧t = true
  ⟦ I.false         ⟧t = false
  ⟦ I.ite t t' t''  ⟧t = ite ⟦ t ⟧t ⟦ t' ⟧t ⟦ t'' ⟧t
\end{code}
\begin{code}[hide]
open import NatBoolWT using (isO)
St : Algebra
St = record
  { Ty     = Set
  ; Nat    = ℕ
  ; Bool   = 𝟚
  ; Con    = Set
  ; ∙      = ↑p 𝟙
  ; _▹_    = _×_
  ; Var    = λ Γ A → Γ → A
  ; vz     = π₂
  ; vs     = λ x γ → x (π₁ γ)
  ; Tm     = λ Γ A → Γ → A
  ; var    = λ x → x
  ; def    = λ u t γ → t (γ , u γ)
  ; zero   = λ γ → O
  ; suc    = λ t γ → S (t γ)
  ; isZero = λ t γ → isO (t γ)
  ; _+_    = λ u v γ → u γ +ℕ v γ
  ; true   = λ γ → I
  ; false  = λ γ → O
  ; ite    = λ t u v γ → if t γ then u γ else v γ
  }
\end{code}
Examples:
\SaveVerb{verb1}|(isZero zero)|
\SaveVerb{verb2}|(if v0 then zero else suc zero)|
\[
 \verb$let $ \underbrace{\UseVerb{verb1}}_{\verb$: Tm ∙ Bool$}\verb$ $\underbrace{\UseVerb{verb2}}_{\verb$: Tm (∙ ▹ Bool) Nat$}\verb$ : Tm ∙ Nat$
\]
\SaveVerb{verb3}|(suc (suc zero))|
\SaveVerb{verb4}|(let |
\SaveVerb{verb5}|v0|
\SaveVerb{verb6}| |
\SaveVerb{verb7}|(isZero (v0 + v1))|
\SaveVerb{verb8}|)|
\SaveVerb{verb9}|Tm (∙ ▹ Nat) Nat|
\SaveVerb{verb10}|Tm (∙ ▹ Nat ▹ Nat) Bool|
\[
 \verb$let $ \underbrace{\UseVerb{verb3}}_{\verb$: Tm ∙ Nat$}\verb$ $\underbrace{\UseVerb{verb4}\underbrace{\UseVerb{verb5}}_{\UseVerb{verb9}}\UseVerb{verb6}\underbrace{\UseVerb{verb7}}_{\UseVerb{verb10}}\UseVerb{verb8}}_{\verb$Tm (∙ ▹ Nat) Bool$}\verb$: Tm ∙ Bool$
\]
