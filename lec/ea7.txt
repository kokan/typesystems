

def x:=3 (x  + def y:=isZero x ) (if y  then  x+1 else  x + 2))

def   3  (v0 + def   (isZero v0) (if v0 then v1+1 else v1 + 2))
         \           \_________/ \___________________________//
          \           : Tm 1             : Tm 2              /
           \________________________________________________/
           : Tm 1


    def
    / \
   /   \
  3     +
       / \
      /   \
     v0    def
          /   \
         /     \
      isZero   ite
         |     /| \
         |    / |  \
         v0  v0 +   +
               /\   /\
             v1 1  v1 2

- egy változónak több különböző De Bruijn index felelhet meg
- a def csak a második paraméterében köt változót



példa De Bruijn indexekre és def használatára:

(1+1) + (((1+1)+(1+1))+((1+1)+(1+1)))

let x:=1+1 in x + ((x+x)+(x+x))

let x:=1+1 in x + let y:=(x+x) in y+y

def (1+1) (v0 + ((v0+v0)+(v0+v0)))
          ^ebben a v0 az (1+1)-et fog jelenti

def (1+1) (v0 + def (v0+v0) (v0+v0))
                            ^ebben a v1 fog (1+1)-et jelenteni
                             ebben a v0 fog ((1+1)+(1+1))-et jeleneti
          ^ebben a v0 az (1+1)-et fog jelenti

= ((1+1) + def ((1+1)+(1+1)) (v0+v0))
= (1+1) + (((1+1)+(1+1))+((1+1)+(1+1)))



def t (v0+v0) = t+t

def t (def t' (v0 + v1)) = t' + t
              ^v0 t'-t jelent, v1 t-t jelent

def t (v0 + def t' (v0 + v1)) = t + (t' + t)

def t (v0 + def t' ((v0 + v1) + def t'' (v0+v1+v2))) =
      ^ ebben a legnagyobb index t-t jelent

       t  + def t' ((v0 +  t) + def t'' (v0+v1+ t))) =
                   ^ ebben a legnagyobb index t'-t jelent

       t  +        ((t' +  t) + def t'' (v0+t'+ t))) =
                                        ^ ebben a legnagyobb index t''-t jelent
       t  +        ((t' +  t) +        (t''+t'+ t))) =


NatBool nyelvre t : I.Tm I.Bool, akkor   t == true vagy t = false
    if true then u else v = u


Def nyelv: benne van a NatBool

def t u = "u-ban a legnagyobb indexet lecseréljük t-re"
   t : Tm Γ A                   t : Tm n
   u : Tm (Γ ▹ A) B             u : Tm (S n)

def t u : Tm Γ B

pl. Γ = ∙ ▹ Bool ▹ Nat ▹ Nat, B=Bool

def t u : Tm (∙ ▹ Bool ▹ Nat ▹ Nat) Bool
      t : Tm (∙ ▹ Bool ▹ Nat ▹ Nat) Nat
      u : Tm (∙ ▹ Bool ▹ Nat ▹ Nat ▹ Nat) Bool

      t = if v2 then v0+1 else v1+2
      u = if isZero v0 then isZero v1 else isZero v2

def t u = def (if v2 then v0+1 else v1+2)
              (if isZero v0 then isZero v1 else isZero v2) =
              (if isZero (if v2 then v0+1 else v1+2) then isZero v0 else isZero v1)

let x:=t in u = u[x↦t]    _[_] helyettesítés alkalmazása

u[x↦t,y↦t']

Con : Set               ∙   : Con
                        _▹_ : Con → Ty → Con
Ty  : Set               Nat, Bool : Ty
Tm  : Con → Ty  → Set
Sub : Con → Con → Set   (helyettesítés, termek listája)     (ÚJ)

Sub Γ Δ : Set, Δ-nyi term, ami mind Γ környezetben van.
ha Δ=∙▹A₁▹A₂▹...▹Aₙ, akkor
Sub Γ Δ ≅ Tm Γ A₁ × Tm Γ A₂ × ... × Tm Γ Aₙ

σ,δ,ν : Sub Γ Δ  (kis görög betűk)

_[_] : Tm Δ A → Sub Γ Δ → Tm Γ A         (helyettesítés alkalmazás)     (ÚJ)
ε    : Sub Γ ∙                           (üres lista)       (ÚJ)
_,_  : Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)  (snoc)             (ÚJ)
id   : Sub Γ Γ                           (identitás)        (ÚJ)
_∘_  : Sub Θ Δ → Sub Γ Θ → Sub Γ Δ       (kompozíció)       (ÚJ)

             σ∘δ
     /------------------
    /                   \
   /    δ          σ     v
   Γ -------> Θ -------> Δ


p    : Sub (Γ▹A) Γ                      (első projekció, gyengítés, weakening)  (ÚJ)
q    : Tm  (Γ▹A) A                      (második projekció)                     (ÚJ)

v0 := q     : Tm (Γ▹A) A
v1 := q[p]  : Tm (Γ▹A▹B) A
v2 := v1[p] : Tm (Γ▹A▹B▹C) A


_[_] : Tm (Γ▹A) A → Sub (Γ▹A▹B) (Γ▹A) → Tm (Γ▹A▹B) A
       ^Δ:=Γ▹A, A:=A, Γ:=Γ▹A▹B
q    : Tm  (Γ▹A) A
p    : Sub (Γ▹A▹B) (Γ▹A)     p-ben Γ:=Γ▹A,A:=B, Agdában p {Γ ▹ A} {B}
q[p] : Tm (Γ▹A▹B) A

_[p] : Tm Γ A → Tm (Γ▹B) A         <- megnöveli a De Bruijn indexeket 1-el
                                   <- gyengíti a termet eggyel

megnéztük az új halmazt (Sub Γ Δ) és az új operátorokat (_[_], ε, _,_, id, _∘_, p, q)
korábbi egyenlőségeink mind igazak:
isZero zero = true
isZero (suc t) = false
zero + v = v
(suc u) + v = suc (u + v)
ite true u v = u
ite false u v = v

minden operátorra (true,false,isZero,...,ite) van egy egyenlőségünk,
amely megmondja, hogy hogyan interaktál a helyettesítéssel:
true [ σ ] = true
false[σ] = false
zero[σ] = zero
(suc t)[σ] = suc (t[σ])
(u+v)[σ] = (u[σ]) + (v[σ])
(ite t u v)[σ] = ite (t[σ]) (u[σ]) (v[σ])
(isZero t)[σ] = isZero (t[σ])
(def t u)[σ] = def (t[σ]) (u[σ∘p,q])
  t : Tm Δ A         t[σ]:Tm Γ A               σ:Sub Γ Δ
  u : Tm (Δ▹A) B     u[σ∘p,q]:Tm (Γ▹A) B      ((σ∘p),q) : Sub (Γ▹A) (Δ▹A)
                                               q   : Tm  (Γ▹A) A
                                               σ∘p : Sub (Γ▹A) Δ
                      p       σ
              (Γ▹A) -----> Γ ---->Δ

a helyettesítések operátorai hogyan interaktálnak egymással (hasonló félcsoporthoz):
idl : id {Δ} ∘ σ  = σ        σ : Sub Γ Δ, id {Δ} : Sub Δ Δ
idr : σ  ∘ id {Γ} = σ
ass : (σ ∘ δ) ∘ ν = σ ∘ (δ ∘ ν)

∙η  : (σ : Sub Γ ∙) → σ = ε
▷β₁ : p ∘ (σ , t) = σ

          (σ,t)         p
       Γ ------> Δ▹A -------> Δ
        \_____________________^
                  σ

▷β₂ : q [σ,t] = t
▷η  : (σ : Sub Γ (Δ▷A)) = (p∘σ,q[σ])
[id] : t[id]=t
[∘]  : t[σ∘δ]=t[σ][δ]

