{-# OPTIONS --prop --rewriting #-}
module gy07_after where

open import Lib

open import DefABT
open import DefWT

module ABT where
  open DefABT.I
  -- Exercise 2.4. Rewrite the following (closed) terms with De Bruijn notation.
  -- (page 28.)

  -- let x:=1 in x + let y:=x+1 in y + let z:=x+y in (x+z)+(y+x)
  a : Tm 0
  a = def (suc zero) (v0 + def (v0 + suc zero) (v0 + def (v1 + v0) ((v2 + v0) + (v1 + v2))))

  -- (let x:=1 in x) + let y:=1 in y + let z:=x+y in (x+z)+(y+x)
  b : Tm 1
  b = (def (suc zero) v0) + def (suc zero) (v0 + def (v1 + v0) ((v2 + v0) + (v1 + v2)))

  -- (let x:=1 in x + let y:=x+1 in y) + let z:=1 in z+z
  c : Tm 0
  c = def (suc zero) (v0 + def (v0 + (suc zero)) v0) + def (suc zero) (v0 + v0)

  -- (let x:=1 in x) + (let y:=1 in y) + let z:=1 in z+z
  d : Tm 0
  d = (def (suc zero) v0) + ((def (suc zero) v0) + (def (suc zero) (v0 + v0)))

  -- let x:=(isZero true) in (ite x zero x)
  e : Tm 0
  e = def (isZero true) (ite v0 zero v0)

  -- CountFree

  zeroVec : (n : ℕ) → Vec ℕ n
  zeroVec O = []
  zeroVec (S n) = O :: zeroVec n

  vec-test-1 = zeroVec 5
  -- 0 :: 0 :: 0 :: 0 :: 0 :: []

  incVec : {n : ℕ} → Var n → Vec ℕ n → Vec ℕ n
  incVec vz (x :: vec) = S x :: vec
  incVec (vs v) (x :: vec) = x :: incVec v vec

  vec-test-2 = incVec (vs (vs (vs vz))) vec-test-1
  -- 0 :: 0 :: 0 :: 1 :: 0 :: []

  unitVec : {n : ℕ} → Var n → Vec ℕ n
  unitVec {n} v = incVec v (zeroVec n)

  vec-test-3 = unitVec {5} (vs vz)
  -- 0 :: 1 :: 0 :: 0 :: 0 :: []

  addVec : {n : ℕ} → Vec ℕ n → Vec ℕ n → Vec ℕ n
  addVec [] [] = []
  addVec (m :: v₁) (n :: v₂) = (m +ℕ n) :: (addVec v₁ v₂)

  vec-test-4 = addVec vec-test-2 vec-test-3
  -- 0 :: 1 :: 0 :: 1 :: 0 :: []

  tail : {n : ℕ} → Vec ℕ (S n) → Vec ℕ n
  tail (x :: v) = v

  vec-test-5 = tail vec-test-4
  -- 1 :: 0 :: 1 :: 0 :: []

  countFree : {n : ℕ} → Tm n → Vec ℕ n
  countFree (var v) = unitVec v
  countFree (def tm tm₁) = addVec (countFree tm) (tail (countFree tm₁))
  countFree {n} zero = zeroVec n
  countFree (suc tm) = countFree tm
  countFree (isZero tm) = countFree tm
  countFree (tm + tm₁) = addVec (countFree tm) (countFree tm₁)
  countFree {n} true = zeroVec n
  countFree {n} false = zeroVec n
  countFree (ite tm tm₁ tm₂) = addVec (countFree tm) (addVec (countFree tm₁) (countFree tm₂))

  -- Test the function using C-c C-n in the hole:
  testCountFree-1 = {!countFree {3} ((v0 + v1) + (v2 + v1))!}
  testCountFree-2 = {!countFree {3} (def (v2) ((v0 + v1) + (v2 + v1)))!}

  -- Weakening

  wkVar : {n : ℕ} → Var n → Var (S n)
  wkVar vz = vs vz
  wkVar (vs v) = vs (wkVar v)

  wkVar-n : {n : ℕ} → (m : ℕ) → Var n → Var (m +ℕ n)
  wkVar-n O v = v
  wkVar-n (S n) v = wkVar (wkVar-n n v)

  wk : {n : ℕ} → Tm n → Tm (S n)
  wk (var x) = var (wkVar x)
  wk (def tm tm₁) = def (wk tm) (wk tm₁)
  wk zero = zero
  wk (suc t₁) = suc (wk t₁)
  wk (isZero t₁) = isZero (wk t₁)
  wk (t₁ + t₂) = wk t₁ + wk t₂
  wk true = true
  wk false = false
  wk (ite t₁ t₂ t₃) = ite (wk t₁) (wk t₂) (wk t₃)

  subTm-1 : Tm 1
  subTm-1 = isZero v0

  subTm-2 : Tm 2
  subTm-2 = isZero v1

  subTm-≡ : wk subTm-1 ≡ subTm-2
  subTm-≡ = refl

  wk-n : {n : ℕ} → (m : ℕ) → Tm n → Tm (m +ℕ n)
  wk-n {n} O tm = tm
  wk-n {n} (S m) tm = wk (wk-n m tm)

module WT where
  open DefWT.I

  -- let x:=1 in x + let y:=x+1 in y + let z:=x+y in (x+z)+(y+x)
  a : Tm ∙ Nat
  a = {!!}

  -- (let x:=1 in x) + let y:=1 in y + let z:=x+y in (x+z)+(y+x)
  b : Tm (∙ ▹ Nat) Nat
  b = {!!}

  -- (let x:=false in (if x then y else (isZero zero)))
  c : Tm (∙ ▹ Bool) Bool
  c = {!!}

  -- (let x:=1 in (if (isZero x) then (let y:=true in y) else y)
  d : Tm ∙ Bool
  d = {!!}

  -- let x:=(isZero zero) in (ite x false x)
  e : Tm ∙ Bool
  e = {!!}

  --

  swapVar : {Γ : Con} → {A B C : Ty} → Var (Γ ▹ A ▹ B) C -> Var (Γ ▹ B ▹ A) C
  swapVar v = {!!}

  wk : {Γ Δ : Con} → {A B : Ty} → Tm (Γ ++ Δ) A -> Tm ((Γ ▹ B) ++ Δ) A
  wk = {!!}

  Std : Algebra
  Std = record
    { Ty     = Set
    ; Nat    = ℕ
    ; Bool   = 𝟚
    ; Con    = Set
    ; ∙      = ↑p 𝟙
    ; _▹_    = _×_
    ; Var    = λ Γ A → Γ → A
    ; vz     = π₂
    ; vs     = λ x γ → x (π₁ γ)
    ; Tm     = λ Γ A → Γ → A
    ; var    = {!!}
    ; def    = {!!}
    ; zero   = {!!}
    ; suc    = {!!}
    ; isZero = {!!}
    ; _+_    = {!!}
    ; true   = {!!}
    ; false  = {!!}
    ; ite    = {!!}
    }
